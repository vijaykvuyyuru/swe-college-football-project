import React, { useState, useEffect} from 'react';
import {Box, Select, MenuItem, FormControl, InputLabel, Divider, Grid, Stack, Card, Button, Autocomplete, TextField} from '@mui/material';
import {useNavigate} from 'react-router-dom';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import {getTuitionBallApi} from './APIServices'
import allSchools from './schoolOptions.json'

function Players(props: any) {

  // Object to help manipulate url parameters
  const params = new URLSearchParams(window.location.search)

  // Object used to navigate to different pages
  const navigation = useNavigate();

  const [playersData, setPlayersData] = useState({} as any);


  const [searchParam, setSearchParam] = useState("");
  
  // Variable to hold the position url param if it exists
  let initialPosition;
  if(params.has('position')){
    initialPosition = params.get('position');
  }
  else{
    initialPosition = "";
  }

  // Variable to hold the year url param if it exists
  let initialClass;
  if(params.has('year')){
    initialClass = params.get('year');
  }
  else{
    initialClass = "";
  }

  // Builds sorting options dictionary
  let dict = new Map<string, string>();
  dict.set("first_name", "First Name A-Z");
  dict.set("first_name-desc", "First Name Z-A");
  dict.set("last_name", "Last Name A-Z");
  dict.set("last_name-desc", "Last Name Z-A");

  // Variable to hold the sort url param if it exists
  let initialSort;
  if(params.has('sort')){
    let value = params.get('sort');
    if(value){
      initialSort = value;
    }
  }
  else 
    initialSort = "";
  
  // On first render, get the players data and search param from url
  useEffect(() => {
          try {
              getTuitionBallApi("players?" + params.toString()).then(res=>{setPlayersData(res)});
              if(params.has('search')){
                let value = params.get('search') + "";
                if(value !== null){
                  setSearchParam(String(value));
                }
              }
          }
          catch (err) {
              console.error(err);
          }
  }, []);

  // Gets the page number from the url params. Returns 1 if there's no page param
  function getPage(){
    if(params.has('page')){
      return Number(params.get('page'));
    }
    return 1
  }

  // Deletes the page parameter from the url if it exists
  function resetPage(){
    if(params.has('page')){
      params.delete('page');
    }
  }

  // Navigates to the previous players page
  function handleBackward(){
    if(getPage() !== 1){
      let newPage = getPage()-1;
      addParam('page', newPage);
    }
  }

  // Navigates to the next players page
  function handleForward(){
    if(getPage() !== playersData.num_pages){
      let newPage = getPage()+1;
      addParam('page', newPage);
    }
  }

  // Navigates to the first players page if not already there
  function handleFirst(){
    if(getPage() !== 1){
      addParam('page', 1);
    }
  }

  // Navigates to the last players page if not already there
  function handleLast(){
    if(getPage() !== playersData.num_pages && playersData.num_pages !== 0){
      addParam('page', playersData.num_pages);
    }
  }

  // Navigates to the player instance page with the provided id
  function handlePlayerClick(id:Number){
    navigation("/PlayersInstance/?id=" + id);
  }

  // Navigates to the school instance page with the provided id
  function handleSchoolClick(id: Number){
    navigation("/SchoolsInstance/?id=" + id);
    window.location.reload();
  }

  // Navigates to the coach instance page with the provided id
  function handleCoachClick(id: Number){
    navigation("/CoachesInstance/?id=" + id);
    window.location.reload();
  }

  // Adds a team param of newValue if newValue != null
  // If newValue == null, delete the team param
  function handleSchoolFilter(newValue: any){
    if(newValue !== null){
      resetPage();
      addParam('team', newValue);
    }
    else{
      params.delete('team');
      navigation("/players/?" + params.toString());
      window.location.reload();
    }
  }

  // Adds a value to the url params and reloads the page
  function addParam(key: any, newValue:any){
    if(params.has(key)){
      params.set(key, newValue);
    }
    else{
      params.append(key, newValue)
    }
    navigation("/players/?" + params.toString());
    window.location.reload();
  }

  // Clears all url params and reloads the page
  function clearParams(){
    navigation("/players");
    window.location.reload();
  }

  // Gets the team provided in the url params or "" if there's no team param
  function getDefaultSchool(){
    if(params.has('team')){
      return params.get('team');
    }
    else{
      return "";
    }
  }

  // Adds a position param of newValue
  function handlePositionFilter(newPosition:any){
    resetPage();
    addParam('position', newPosition);
  }

  // Adds a year param of newValue
  function handleClassFilter(newClass:any){
    resetPage();
    addParam('year', newClass);
  }

  // Adds a sort param of newValue
  function handleSort(newValue: any){
    addParam('sort', newValue);
  }

  // Resets the search parameter
  function handleChange(event: any){
    setSearchParam(event.target.value);
  }

  // Handle's enter key press on search bar
  function handleSearch(event: any){
    if(event.key === 'Enter'){
      var p = new URLSearchParams();
      if(p.has('search')){
        p.set('search', event.target.value);
      }
      else{
        p.append('search', event.target.value)
      }
      navigation("/players/?" + p.toString());
      window.location.reload();
    }
  }

  // this function searches through the text parameter and highlights the section
  // that equals the highlight parameter while leaving the rest of the text as is
  const Highlighted = ({ text = "", highlight = "" }) => {
    if (!highlight.trim()) {
      return <span>{text}</span>;
    }
    const regex = new RegExp(`(${highlight})`, "gi");
    const parts = text.split(regex);
  
    return (
      <span>
        {parts.filter(String).map((part, i) => {
          return regex.test(part) ? (
            <mark key={i}>{part}</mark>
          ) : (
            <span key={i}>{part}</span>
          );
        })}
      </span>
    );
  };

  // Sets default values for school filter
  let defaultSchool = getDefaultSchool();
  
  // Builds the Players card grid
  let grid = (<Typography variant="h5" component="div" fontWeight="bold"> LOADING... </Typography>);
  if(playersData.data){
      grid = playersData.data.map((playerData : any) => (
      <Grid item xs={3} sx={{display:"flex", justifyContent:"center"}}>
      <Card sx={{ width: 280, ':hover': { boxShadow: 20 }, border:"solid lightgray 1px"}}>
        <CardActionArea onClick={()=>handlePlayerClick(playerData.id)}> 
          <CardMedia
            component="img"
            height="370"
            width="auto"
            image={playerData.picture}
            alt={playerData.first_name + " " + playerData.last_name}
          />
          <CardContent sx={{backgroundColor:"#FAF9F6", borderTop:"solid gray 2px"}}>
            <Typography gutterBottom variant="h5" component="div" fontWeight="bold">
              {params.has('search') ? <Highlighted text={String(playerData.first_name + " " + playerData.last_name)} highlight={String(params.get('search'))}/> : playerData.first_name + " " + playerData.last_name}
            </Typography>
              <ul style={{ listStyleType:'none', margin:"0", padding:"0"}}>
                <li key={playerData.id} style={{color:'blue', textDecoration:'underline'}} onClick={()=> handleSchoolClick(playerData.school_id)}>School: {params.has('search') ? <Highlighted text={String(playerData.school)} highlight={String(params.get('search'))}/> : playerData.school}</li>
                <li key={-1} style={{color:'blue', textDecoration:'underline'}} onClick={()=> handleCoachClick(playerData.coach_id)}>Coach: {params.has('search') ? <Highlighted text={String(playerData.coach_first_name + " " + playerData.coach_last_name)} highlight={String(params.get('search'))}/> : playerData.coach_first_name + " " + playerData.coach_last_name}</li>
                <li key={-2}>Position: {params.has('search') ? <Highlighted text={String(playerData.position)} highlight={String(params.get('search'))}/> : playerData.position}</li>
                <li key={-3}>Number: {params.has('search') ? <Highlighted text={String(playerData.jersey)} highlight={String(params.get('search'))}/> : playerData.jersey}</li>
                <li key={-4}>Class: {params.has('search') ? <Highlighted text={String(playerData.year)} highlight={String(params.get('search'))}/> : playerData.year}</li>
              </ul>
          </CardContent>
        </CardActionArea>
      </Card>
      </Grid>
    ));
  }
    
    

  return (
  
    <div className="PlayerModel">
      <Stack
        direction="column"
        justifyContent="center"
        alignItems="center"
        spacing={2}
        marginBottom={2}
      >
        <h1 style={{textAlign:"center"}}>
          Search for players!
        </h1>
        
        {/* Seach Bar  */}
        <Box sx={{display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '0px 0px 0px 0px'}}>
          <FormControl sx={{width: '75vw'}}>
            <TextField id="outlined-basic" label="Search" variant="outlined" value = {searchParam} onChange = {handleChange} onKeyDown = {handleSearch} />
          </FormControl>
        </Box>

        {/* Filtering and Sorting  */}
        <Grid sx ={{ justifyContent: 'center', alignItems: 'center'}} container spacing={2}>
          <Grid item xs="auto">
            <h2 style = {{textAlign:'left'}}>Filter</h2>
          </Grid>
          {/* School Filter  */}
          <Grid item xs="auto">
            <Box sx={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
              <Autocomplete
                  disablePortal
                  id="school-select"
                  defaultValue = {defaultSchool}
                  onChange={(event, newSchool) => handleSchoolFilter(newSchool)}
                  options={allSchools.schools.slice(1, allSchools.schools.length).map((school) => school.label)}
                  sx={{ width: 300 }}
                  renderInput={(params) => <TextField {...params} label="School" />}
              />
            </Box>
          </Grid>

          {/* Position Filter  */}
          <Grid item xs="auto">
            <Box sx={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
              <FormControl sx={{ minWidth: 100 }}>
                <InputLabel id="demo-simple-select-label">Position</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  label="Position"
                  value={initialPosition}
                  onChange={(event) => handlePositionFilter(event.target.value)}
                >
                  <MenuItem value={"QB"}>QB</MenuItem>
                  <MenuItem value={"RB"}>RB</MenuItem>
                  <MenuItem value={"WR"}>WR</MenuItem>
                  <MenuItem value={"TE"}>TE</MenuItem>
                </Select>
              </FormControl>
            </Box>
          </Grid>

          {/* Class Filter  */}
          <Grid item xs="auto">
            <Box sx={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
              <FormControl sx={{ m: 1, minWidth: 100 }}>
                <InputLabel id="demo-simple-select-label">Class</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  label="Class"
                  value={initialClass}
                  onChange={(event) => handleClassFilter(event.target.value)}
                >
                  <MenuItem value={"freshman"}>Freshman</MenuItem>
                  <MenuItem value={"sophomore"}>Sophomore</MenuItem>
                  <MenuItem value={"junior"}>Junior</MenuItem>
                  <MenuItem value={"senior"}>Senior</MenuItem>
                </Select>
              </FormControl>
            </Box>
          </Grid>

          <Grid item xs="auto">
            <h2 style = {{textAlign:'left'}}>Sort</h2>
          </Grid>

          {/* Sorting Dropdown  */}
          <Grid item xs="auto">
            <Box sx={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
              <FormControl sx={{ m: 1, minWidth: 100 }}>
                <InputLabel id="demo-simple-select-label">Sort By</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  label="Sort By"
                  value={initialSort}
                  onChange={(event) => handleSort(event.target.value)}
                >
                  <MenuItem value={"first_name"}>First Name A-Z</MenuItem>
                  <MenuItem value={"first_name-desc"}>First Name Z-A</MenuItem>
                  <MenuItem value={"last_name"}>Last Name A-Z</MenuItem>
                  <MenuItem value={"last_name-desc"}>Last Name Z-A</MenuItem>
                </Select>
              </FormControl>
            </Box>
          </Grid>
        </Grid>

        {/* Clear Filters Button  */}
        <Button onClick={clearParams}>Clear Filters</Button>
      </Stack>
      <Divider variant="middle" sx={{backgroundColor: 'gray'}}/>
      
      {/* Players Card Grid  */}
      <Grid
            container
            spacing={2}
            display="flex"
            justifyContent="center"
            margin="auto"
            direction="row"
            alignItems="flex-start"
        >
        {grid}
      </Grid>
      
      {/* Pagination Buttons */}
      <Stack
        direction="row"
        display="flex"
        justifyContent="center"
        alignItems="center"
        marginTop={4}
        spacing={4}
      >
        <Button onClick={handleFirst} variant="contained">{'<<'}</Button>
        <Button onClick={handleBackward} variant="contained">{'<'}</Button>
        <Typography gutterBottom variant="h5" component="div" fontWeight="bold">Page {String(getPage())}/{playersData.num_pages}</Typography>
        <Button onClick={handleForward} variant="contained">{'>'}</Button>
        <Button onClick={handleLast} variant="contained">{'>>'}</Button>
      </Stack>

      <Typography sx={{display:"flex", justifyContent:"center", marginTop:"25px"}}gutterBottom variant="h5" component="div" fontWeight="bold">Total Number of Instances: {playersData.total_num_records}</Typography>
    </div>
  );
};

export default Players;