import React from 'react';
import { Link } from "react-router-dom";
import { Toolbar, Box, AppBar, Button } from '@mui/material';

import { styled } from '@mui/system';

const StyledAppBar = styled(AppBar)({
  backgroundColor: '#bf5700',
});


const Navbar = (props:any) => {

  return (
    <>
      <Box sx={{ display: 'flex' }}>
        <StyledAppBar
          position="fixed"
        >
          <Toolbar sx={{ justifyContent: 'space-between' }}>
            <Button
              component={Link}
              to="/"
              color="secondary"
              sx={{ textTransform: 'none' }}
            >
              TuitionBall
            </Button>
            <Box sx={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap', paddingLeft: '8px' }}>
              <Button
                component={Link}
                to="/theBasics"
                color="secondary"
              >
                The Basics
              </Button>
              <Button
                component={Link}
                to="/schools"
                color="secondary"
              >
                Schools
              </Button>
              <Button
                component={Link}
                to="/coaches"
                color="secondary"
              >
                Coaches
              </Button>
              <Button
                component={Link}
                to="/players"
                color="secondary"
              >
                Players
              </Button>
              <Button
                component={Link}
                to="/visualizations"
                color="secondary"
              >
                Visualizations
              </Button>
              <Button
                component={Link}
                to="/visualizationsProvider"
                color="secondary"
              >
                Provider Visualizations
              </Button>
              <Button
                component={Link}
                to="/about"
                color="secondary"
              >
                About Us
              </Button>
            </Box>
          </Toolbar>
        </StyledAppBar>
        <Toolbar />
      </Box>
    </>
  );
}

export default Navbar;