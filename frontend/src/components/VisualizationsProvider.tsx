import React from 'react';
import { RadarChart, PolarGrid, Radar, PolarAngleAxis, PolarRadiusAxis, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer,  PieChart, Pie, BarChart, Bar } from 'recharts';
import { Box, Typography, Stack } from '@mui/material';
import recipeData from './VisualizationData/recipeData.json'
import cookTimeData from './VisualizationData/cookTimeData.json'
import categoryData from './VisualizationData/categoryData.json'

class VisualizationsProvider extends React.Component {  
    render() {
        return (
            <Stack
            direction="column"
            justifyContent="center"
            alignItems="center"
            spacing={2}
            marginTop={4}
            >
                
                <Box sx={{
                display: 'flex',
                alignItems: 'center',
                width: '100%',
                flexDirection: 'column'
                }}
                >
                    <Typography variant='h4' sx={{ paddingTop: '32px' }}>
                        Total Recipes by Subregion
                    </Typography>      
                    <ResponsiveContainer width="100%" height={900}>
                        <PieChart width={400} height={400}>
                        <Pie
                            dataKey="recipes"
                            nameKey="subregion"
                            isAnimationActive={true}
                            data={recipeData}
                            cx="50%"
                            cy="50%"
                            outerRadius={300}
                            fill="#82ca9d"
                            label
                        />
                        <Tooltip />
                        </PieChart>
                    </ResponsiveContainer>
                </Box>

                <Box sx={{
                display: 'flex',
                alignItems: 'center',
                width: '100%',
                flexDirection: 'column'
                }}
                >
                    <Typography variant='h4' sx={{ paddingTop: '32px' }}>
                        Number of Recipes by Preparation Time (Minutes)
                    </Typography>      
                    <ResponsiveContainer width="60%" height={800}>
                    <BarChart width={400} height={400} data={cookTimeData}>
                      <CartesianGrid strokeDasharray="3 3" />
                      <XAxis dataKey="time" />
                      <YAxis/>
                      <Tooltip />
                      <Bar dataKey="Number of Recipes" fill="#8884d8" />
                    </BarChart>
                    </ResponsiveContainer>
                </Box>

                <Box sx={{
                display: 'flex',
                alignItems: 'center',
                width: '100%',
                flexDirection: 'column'
                }}
                >
                    <Typography variant='h4' sx={{ paddingTop: '32px' }}>
                        Number of Recipes by Category
                    </Typography>      
                    <ResponsiveContainer width="60%" height={800}>
                    <RadarChart outerRadius={300} width={300} height={250} data={categoryData}>
                      <PolarGrid />
                      <PolarAngleAxis dataKey="category" />
                      <PolarRadiusAxis angle={30} domain={[0, 45]} />
                      <Radar name="Number of Recipes" dataKey="Number of Recipes" stroke="red" fill="orange" fillOpacity={0.6} />
                      <Tooltip />
                    </RadarChart>
                    </ResponsiveContainer>
                </Box>
            </Stack>
                
        );
    };
}

export default VisualizationsProvider;