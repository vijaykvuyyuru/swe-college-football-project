import React from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer,  PieChart, Pie, BarChart, Bar } from 'recharts';
import { Box, Typography, Stack } from '@mui/material';
import coachesData from './VisualizationData/coachesData.json'
import schoolsData from './VisualizationData/conferenceData.json'
import playersData from './VisualizationData/playersData.json'

class Visualizations extends React.Component {  
    render() {
        return (
            <Stack
            direction="column"
            justifyContent="center"
            alignItems="center"
            spacing={2}
            marginTop={4}
            >
                <Box sx={{
                display: 'flex',
                alignItems: 'center',
                width: '100%',
                flexDirection: 'column'
                }}
                >
                    <Typography variant='h4' sx={{ paddingTop: '32px' }}>
                        Wins Per Season by Coach
                    </Typography>      
                    <ResponsiveContainer width="80%" height={600}>
                        <LineChart width={200} height={400}>
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="year" type="number" allowDuplicatedCategory={false} domain={[1990, 2021]}/>
                        <YAxis dataKey="value" unit=" wins"/>
                        <Tooltip />

                        {coachesData.map((s) => (
                            <Line dataKey="value" data={s.data} name={s.name} key={s.name} stroke={s.color}/>
                        ))}
                        </LineChart>
                    </ResponsiveContainer>
                </Box>
                <Box sx={{
                display: 'flex',
                alignItems: 'center',
                width: '100%',
                flexDirection: 'column'
                }}
                >
                    <Typography variant='h4' sx={{ paddingTop: '32px' }}>
                        Total Tuition Cost by NCAA Conference
                    </Typography>      
                    <ResponsiveContainer width="100%" height={900}>
                        <PieChart width={400} height={400}>
                        <Pie
                            dataKey="cost"
                            isAnimationActive={true}
                            data={schoolsData}
                            cx="50%"
                            cy="50%"
                            outerRadius={300}
                            fill="#8884d8"
                            label
                        />
                        <Tooltip />
                        </PieChart>
                    </ResponsiveContainer>
                </Box>
                <Box sx={{
                display: 'flex',
                alignItems: 'center',
                width: '100%',
                flexDirection: 'column'
                }}
                >
                    <Typography variant='h4' sx={{ paddingTop: '32px' }}>
                        Frequency of Jersey Numbers
                    </Typography>      
                    <ResponsiveContainer width="80%" height={600}>
                        <BarChart width={200} height={400} data={playersData}>
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="jersey_number"/>
                        <YAxis dataKey="frequency"/>
                        <Tooltip />
                        <Bar dataKey="frequency" fill="#82ca9d"/>
                        </BarChart>
                    </ResponsiveContainer>
                </Box>
                </Stack>
                
        );
    };
}

export default Visualizations;