import { createTheme } from '@mui/material/styles';

export const theme = createTheme({
  palette: {
    primary: {
      main: '#bf5700',
    },
    secondary: {
      main: '#FFFFFF',
    },
  },
});
