interface MemberJSON {
    Name:       string;
    Role:       string;
    Picture:    string;
    Bio:        string;
    Username:   string;
    Email:      string;
    NumCommits: number;
    NumIssues:  number;
    NumTests:   number;
  }

const teamData: MemberJSON[] = [
    {
        Name: "Abhinav Mugunda",
        Role: "Frontend Developer",
        Picture: "abhinav_headshot.jpg",
        Bio: "Abhinav Mugunda is a junior Computer Science major at UT Austin. His free time is spent playing basketball, rock climbing, and spending time with his friends and family.",
        Username: "am85942",
        Email: "am85942@utexas.edu",
        NumCommits: 0,
        NumIssues: 0,
        NumTests: 0,
    },
    {
        Name: "Blake Chambers",
        Role: "Frontend Developer & Phase 4 Leader",
        Picture: "blake_headshot.jpg",
        Bio: "Blake Chambers is a junior Computer Science major at UT Austin. He enjoys watching movies, reading, and going to sporting events in his free time.",
        Username: "blakeC30",
        Email: "chambersbw@gmail.com",
        NumCommits: 0,
        NumIssues: 0,
        NumTests: 0,
    },
    {
        Name: "Brock Moore",
        Role: "Frontend Developer & Phase 3 Leader",
        Picture: "brock_headshot.jpg",
        Bio: "Brock Moore is a senior Computer Science major at UT Austin. He enjoys playing video games and golfing in his free time.",
        Username: "brockm2018",
        Email: "brockm2018@gmail.com",
        NumCommits: 0,
        NumIssues: 0,
        NumTests: 0,
    },
    {
        Name: "Matt Dombrowski",
        Role: "Backend Developer & Phase 2 Leader",
        Picture: "matt_headshot.jpg",
        Bio: "Matt Dombrowski is a senior Computer Science major at UT Austin. He likes playing guitar and going on hiking trips in his freetime.",
        Username: "mattdombrowski21",
        Email: "matt.dombrowski21@gmail.com",
        NumCommits: 0,
        NumIssues: 0,
        NumTests: 15,
    },
    {
        Name: "Vijay Vuyyuru",
        Role: "Backend Developer & Phase 1 Leader",
        Picture: "vijay_headshot.jpg",
        Bio: "Vijay Vuyyuru is a junior Computer Science major at UT Austin. Outside of class he plays videogames and bakes.",
        Username: "vijaykvuyyuru",
        Email: "vijaykvuyyuru@gmail.com",
        NumCommits: 0,
        NumIssues: 0,
        NumTests: 41,
    },
];

export type { MemberJSON };
export { teamData };

