import React, { useState, useEffect} from 'react';
import {Box, Select, MenuItem, FormControl, InputLabel, Divider, Grid, Stack, Card, Button, Autocomplete, TextField} from '@mui/material';
import {useNavigate} from 'react-router-dom'
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import {getTuitionBallApi} from './APIServices'
import allSchools from './schoolOptions.json'

export const Coaches = () => {
  // Object to help manipulate url parameters
  const params = new URLSearchParams(window.location.search)
  
  // Object used to navigate to different pages
  const navigation = useNavigate();

  const [coachesData, setCoachesData] = useState({} as any);

  const [searchParam, setSearchParam] = useState("");

  const current_year = new Date().getFullYear();

  // Builds options for hired before and after filters
  var dateOptions:String[] = [];
  for(var i=current_year; i > 1997; i--){
    dateOptions.push("" + i);
  }

  // Builds sorting options dictionary
  let dict = new Map<string, string>();
  dict.set("first_name", "First Name A-Z");
  dict.set("first_name-desc", "First Name Z-A");
  dict.set("last_name", "Last Name A-Z");
  dict.set("last_name-desc", "Last Name Z-A");
  dict.set("salary", "Salary Ascending");
  dict.set("salary-desc", "Salary Descending");

  // Variable to hold the sort url param if it exists
  let initialSort;
  if(params.has('sort')){
    let value = params.get('sort');
    if(value){
      initialSort = value;
    }
  }
  else 
    initialSort = "";

  // On first render, get the coaches data and search param from url
  useEffect(() => {
          try {
              getTuitionBallApi("coaches?sparse=true&" + params.toString()).then(res=>{setCoachesData(res)});
              if(params.has('search')){
                let value = params.get('search') + "";
                if(value !== null){
                  setSearchParam(String(value));
                }
              }
          }
          catch (err) {
              console.error(err);
          }
  }, []);

  // Adds a value to the url params and reloads the page
  function addParam(key: any, newValue:any){
    if(params.has(key)){
      params.set(key, newValue);
    }
    else{
      params.append(key, newValue)
    }
    navigation("/coaches/?" + params.toString());
    window.location.reload();
  }

  // Gets the page number from the url params. Returns 1 if there's no page param
  function getPage(){
    if(params.has('page')){
      return Number(params.get('page'));
    }
    return 1
  }

  // Deletes the page parameter from the url if it exists
  function resetPage(){
    if(params.has('page')){
      params.delete('page');
    }
  }

  // Navigates to the previous coaches page
  function handleBackward(){
    if(getPage() !== 1){
      let newPage = getPage()-1;
      addParam('page', newPage);
    }
  }

  // Navigates to the next coaches page
  function handleForward(){
    if(getPage() !== coachesData.num_pages){
      let newPage = getPage()+1;
      addParam('page', newPage);
    }
  }

  // Navigates to the first coaches page if not already there
  function handleFirst(){
    if(getPage() !== 1){
      addParam('page', 1);
    }
  }

  // Navigates to the last coaches page if not already there
  function handleLast(){
    if(getPage() !== coachesData.num_pages && coachesData.num_pages !== 0){
      addParam('page', coachesData.num_pages);
    }
  }

  // Navigates to the coach instance page with the provided id
  function handleCoachClick(id: any){
    navigation("/CoachesInstance/?id=" + id);
  }

  // Navigates to the school instancee page with the provided id
  function handleSchoolClick(id: Number){
    navigation("/SchoolsInstance/?id=" + id);
    window.location.reload();
  }

  // Gets the team provided in the url params or "" if there's no team param
  function getDefaultSchool(){
    if(params.has('team')){
      return params.get('team');
    }
    else{
      return "";
    }
  }

  // Gets the year specified in the hired_for url param or "" if there's no hired_before param
  function getDefaultHiredBefore(){
    if(params.has('hired_before')){
      let value = params.get('hired_before');
      if(value){
        return value.split('-')[0];
      }
    }
    else{
      return "";
    }
  }
  
  // Gets the year specified in the hired_after url param or "" if there's no hired_after param
  function getDefaultHiredAfter(){
    if(params.has('hired_after')){
      let value = params.get('hired_after');
      if(value){
        return value.split('-')[0];
      }
    }
    else{
      return "";
    }
  }

  // Clears all url params and reloads the page
  function clearParams(){
    navigation("/coaches");
    window.location.reload();
  }

  // Adds a team param of newValue if newValue != null
  // If newValue == null, delete the team param
  function handleSchoolFilter(newValue: any){
    if(newValue !== null){
      resetPage();
      addParam('team', newValue);
    }
    else{
      params.delete('team');
      navigation("/coaches/?" + params.toString());
      window.location.reload();
    }
  }

  // Adds a hired_before param of newValue if newValue != null
  // If newValue == null, delete the hired_before param
  function handleHiredBeforeFilter(newYear:any){
    if(newYear !== null){
      let newDate = newYear + "-01-01T10:10:00";
      resetPage();
      addParam('hired_before', newDate);
    }
    else{
      params.delete('hired_before');
      navigation("/coaches/?" + params.toString());
      window.location.reload();
    }
  }

  // Adds a hired_after param of newValue if newValue != null
  // If newValue == null, delete the hired_after param
  function handleHiredAfterFilter(newYear:any){
    if(newYear !== null){
      let newDate = newYear + "-01-01T10:10:00";
      resetPage();
      addParam('hired_after', newDate);
    }
    else{
      params.delete('hired_after');
      navigation("/coaches/?" + params.toString());
      window.location.reload();
    }
  }

  // Adds a sort param of newValue
  function handleSort(newValue: any){
    addParam('sort', newValue);
  }

  // Resets the search parameter
  function handleChange(event: any){
    setSearchParam(event.target.value);
  }

  // Handle's enter key press on search bar
  function handleSearch(event: any){
    if(event.key === 'Enter'){
      var p = new URLSearchParams();
      if(p.has('search')){
        p.set('search', event.target.value);
      }
      else{
        p.append('search', event.target.value)
      }
      navigation("/coaches/?" + p.toString());
      window.location.reload();
    }
  }

  // this function searches through the text parameter and highlights the section
  // that equals the highlight parameter while leaving the rest of the text as is
  const Highlighted = ({ text = "", highlight = "" }) => {
    if (!highlight.trim()) {
      return <span>{text}</span>;
    }
    const regex = new RegExp(`(${highlight})`, "gi");
    const parts = text.split(regex);
  
    return (
      <span>
        {parts.filter(String).map((part, i) => {
          return regex.test(part) ? (
            <mark key={i}>{part}</mark>
          ) : (
            <span key={i}>{part}</span>
          );
        })}
      </span>
    );
  };

  // Sets default values for filters
  let defaultSchool = getDefaultSchool();
  let defaultHiredBefore = getDefaultHiredBefore();
  let defaultHiredAfter = getDefaultHiredAfter();

  // Builds the Coaches card grid
  let grid = (<Typography variant="h5" component="div" fontWeight="bold"> LOADING... </Typography>);
    if(coachesData.data){
        grid = coachesData.data.map((coachData : any) => (
        <Grid item xs={3} sx={{display:"flex", justifyContent:"center"}}>
        <Card sx={{ width: 280, ':hover': { boxShadow: 20 }, border:"solid lightgray 1px"}}>
          <CardActionArea onClick={()=>handleCoachClick(coachData.id)}> 
            <CardMedia
              component="img"
              height="370"
              width="auto"
              image={coachData.image}
              alt={coachData.first_name + " " + coachData.last_name}
            />
            <CardContent sx={{backgroundColor:"#FAF9F6", borderTop:"solid gray 2px"}}>
               <Typography gutterBottom variant="h5" component="div" fontWeight="bold">
                {params.has('search') ? <Highlighted text={String(coachData.first_name + " " + coachData.last_name)} highlight={String(params.get('search'))}/> : coachData.first_name + " " + coachData.last_name}
               </Typography>
                <ul style={{ listStyleType:'none', margin:"0", padding:"0"}}>
                   <li style={{color:'blue', textDecoration:'underline'}} onClick={()=> handleSchoolClick(coachData.school_id)}>School: {params.has('search') ? <Highlighted text={String(coachData.Current_school)} highlight={String(params.get('search'))}/> : coachData.Current_school}</li>
                   <li>Overall Record: {coachData.overall_wins + "-" + coachData.overall_losses}</li>
                   <li>Current Record: {coachData.current_wins + "-" + coachData.current_losses}</li>
                   <li>Salary: ${(coachData.salary === "Not found" ? "1500000" : coachData.salary)}</li>
                 </ul>
            </CardContent>
           </CardActionArea>
         </Card>
         </Grid>
       ));
    }


    return (
      <div className="CoachesModel">
        <Stack
          direction="column"
          justifyContent="center"
          alignItems="center"
          spacing={2}
          marginBottom={2}
        >
          <h1 style={{textAlign:"center"}}>
            Search for Coaches!
          </h1>
          
          {/* Search Bar */}
          <Box sx={{display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '0px 0px 0px 0px'}}>
            <FormControl sx={{width: '75vw'}}>
            <TextField id="outlined-basic" label="Search" variant="outlined" value = {searchParam} onChange = {handleChange} onKeyDown = {handleSearch} />
            </FormControl>
          </Box>

          {/* Filtering and Sorting*/}
          <Grid sx ={{ justifyContent: 'center', alignItems: 'center'}} container spacing={2}>
            <Grid item xs="auto">
              <h2 style = {{textAlign:'left'}}>Filter</h2>
            </Grid>

            {/* School Filter */}
            <Grid item xs="auto">
              <Box sx={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                <Autocomplete
                    disablePortal
                    id="school-select"
                    defaultValue = {defaultSchool}
                    onChange={(event, newSchool) => handleSchoolFilter(newSchool)}
                    options={allSchools.schools.slice(1, allSchools.schools.length).map((school) => school.label)}
                    sx={{ width: 300 }}
                    renderInput={(params) => <TextField {...params} label="School" />}
                />
              </Box>
            </Grid>
            
            {/* Hired After Filter  */}
            <Grid item xs="auto">
              <Box sx={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                <Autocomplete
                    disablePortal
                    id="school-select"
                    defaultValue = {defaultHiredAfter}
                    onChange={(event, newYear) => handleHiredAfterFilter(newYear)}
                    options={dateOptions}
                    sx={{ width: 150 }}
                    renderInput={(params) => <TextField {...params} label="Hired After" />}
                />
              </Box>
            </Grid>

            {/* Hired Before Filter  */}
            <Grid item xs="auto">
              <Box sx={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                <Autocomplete
                    disablePortal
                    id="school-select"
                    defaultValue = {defaultHiredBefore}
                    onChange={(event, newYear) => handleHiredBeforeFilter(newYear)}
                    options={dateOptions}
                    sx={{ width: 165 }}
                    renderInput={(params) => <TextField {...params} label="Hired Before" />}
                />
              </Box>
            </Grid>

            
            <Grid item xs="auto">
              <h2 style = {{textAlign:'left'}}>Sort</h2>
            </Grid>

            {/* Sort Title and Dropdown  */}
            <Grid item xs="auto">
              <Box sx={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                <FormControl sx={{ m: 1, minWidth: 100 }}>
                  <InputLabel id="demo-simple-select-label">Sort By</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    label="Sort By"
                    value={initialSort}
                    onChange={(event) => handleSort(event.target.value)}
                  >
                    <MenuItem value={"first_name"}>First Name A-Z</MenuItem>
                    <MenuItem value={"first_name-desc"}>First Name Z-A</MenuItem>
                    <MenuItem value={"last_name"}>Last Name A-Z</MenuItem>
                    <MenuItem value={"last_name-desc"}>Last Name Z-A</MenuItem>
                    <MenuItem value={"salary"}>Salary Ascending</MenuItem>
                    <MenuItem value={"salary-desc"}>Salary Descending</MenuItem>
                  </Select>
                </FormControl>
              </Box>
            </Grid>
          </Grid>

          {/* Clear Filters Button  */}
          <Button onClick={clearParams}>Clear Filters</Button>
          </Stack>

          <Divider variant="middle" sx={{backgroundColor: 'gray'}}/>

          {/* Grid of Coaches Cards  */}
          <Grid
                container
                spacing={2}
                display="flex"
                justifyContent="center"
                margin="auto"
                direction="row"
                alignItems="flex-start"
            >
            {grid}

          </Grid>
          
          {/* Pagination Buttons  */}
          <Stack
          direction="row"
          display="flex"
          justifyContent="center"
          alignItems="center"
          marginTop={4}
          spacing={4}
          >
            <Button onClick={handleFirst} variant="contained">{'<<'}</Button>
            <Button onClick={handleBackward} variant="contained">{'<'}</Button>
            <Typography gutterBottom variant="h5" component="div" fontWeight="bold">Page {String(getPage())}/{coachesData.num_pages}</Typography>
            <Button onClick={handleForward} variant="contained">{'>'}</Button>
            <Button onClick={handleLast} variant="contained">{'>>'}</Button>
          </Stack>

          <Typography sx={{display:"flex", justifyContent:"center", marginTop:"25px"}}gutterBottom variant="h5" component="div" fontWeight="bold">Total Number of Instances: {coachesData.total_num_records}</Typography>
         
      </div>
      

    )
}