import { Box, Typography, Stack, Card, CardHeader, Link } from '@mui/material';
import { MemberJSON } from './TeamData'
import { Tool, toolsUsed } from './ToolsData'
import { getGitlabStats } from './APIServices';
import { useEffect, useState } from 'react'

const CardImage = ({
    display: 'flex',
    height: '200px',
    marginTop: '10px',
    marginLeft: 'auto',
    marginRight: 'auto',
});

const Headshot = ({
    display: 'flex',
    height: '200px',
    width: '200px',
    overflow: 'hidden',
    marginTop: '10px',
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: '25%',
    boxShadow: "2px 3px 6px gray",
});

export const About = () => {
    const [members, setMembers] = useState<MemberJSON[]>([])
    const [commits, setCommits] = useState<number>(0)
    const [issues, setIssues] = useState<number>(0)
    const [tests, setTests] = useState<number>(0)

    useEffect(() => {
        getGitlabStats().then(res => {
            let totalCommits: number = 0
            let totalIssues: number = 0
            let totalTests: number = 0
            res.forEach(function (member) {
                totalCommits += member.NumCommits
                totalIssues += member.NumIssues
                totalTests += member.NumTests
            })

            setMembers(res)
            setCommits(totalCommits)
            setIssues(totalIssues)
            setTests(totalTests)
        })
    }, [])

    return (
        <Stack spacing={4}>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h2" sx={{ paddingLeft: '16px' }}><b>About Us</b></Typography>
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography align='left' sx={{ paddingLeft: '100px', paddingRight: '100px' }}>Football can be a massive part of campus culture depending on which university you attend. Do you want to see how your team stacks up, or do you wish to see how football statistics may be affected by tuition rates and coach salaries? You've come to the right place! Search for players, schools, and coaches across the NCAA division 1.</Typography>
            </Box>

            <Box sx={{ display: 'flex', justifyContent: 'center', maxWidth: '100%', marginLeft: "16px", marginRight: "16px"}}>
                <Typography variant="h4"><b>Meet the Team</b></Typography>
            </Box>

            <Box sx={{
                display: 'flex',
                flexWrap: 'wrap',
                flexDirection: 'row',
                justifyContent: 'center',
                maxWidth: '100%',
                marginLeft: "16px",
                marginRight: "16px",
            }}>
                {members.map((member) => {
                    return (
                        <Card key={member.Name} sx={{ width: '400px', margin: '5px', ':hover': { boxShadow: 20 } }}>
                            <img src={require(`./media/${member.Picture}`)} alt="Tutorial" title={member.Name} style={Headshot} />
                            <CardHeader
                                title={member.Name + " - " + member.Role}
                                subheader={member.Bio}
                            />
                            <Typography variant='subtitle1' sx={{ paddingLeft: '15px' }}>Number of Commits: {member.NumCommits}</Typography>
                            <Typography variant='subtitle1' sx={{ paddingLeft: '15px' }}>Number of Closed Issues: {member.NumIssues}</Typography>
                            <Typography variant='subtitle1' sx={{ paddingLeft: '15px' }}>Number of Unit Tests: {member.NumTests}</Typography>
                        </Card>
                    )
                })}
            </Box>

            <Box sx={{
                display: 'flex',
                flexWrap: 'wrap',
                flexDirection: 'row',
                justifyContent: 'center',
                maxWidth: '100%',
                marginLeft: "16px",
                marginRight: "16px",
            }}>
                <Link style={{ textDecoration: 'none' }} href="https://gitlab.com/vijaykvuyyuru/swe-college-football-project/-/tree/main" target="_blank" rel="noopener noreferrer">
                    <Card key="GitLab Repository"
                        sx={{ width: '300px', height: '95%', marginLeft: '5px', marginRight: '5px', paddingBottom: '10px', ':hover': { boxShadow: 20 } }}>
                        <img
                            src={`https://cdn.icon-icons.com/icons2/2415/PNG/512/gitlab_original_logo_icon_146503.png`}
                            srcSet={`https://cdn.icon-icons.com/icons2/2415/PNG/512/gitlab_original_logo_icon_146503.png`}
                            alt="GitLab Repository"
                            loading="lazy"
                            style={CardImage}
                        />
                        <CardHeader title="Our GitLab Repository" />
                        <Typography display="block" sx={{ paddingLeft: '5px' }}><b>Total Commits: </b> {commits}</Typography>
                        <Typography display="block" sx={{ paddingLeft: '5px' }}><b>Total Issues: </b> {issues}</Typography>
                        <Typography display="block" sx={{ paddingLeft: '5px' }}><b>Total Tests: </b> {tests}</Typography>
                    </Card>
                </Link>

                <Link style={{ textDecoration: 'none' }} href="https://documenter.getpostman.com/view/19756764/UVkpQGPx" target="_blank" rel="noopener noreferrer">
                    <Card key={"Postman Documentation"}
                        sx={{ width: '300px', height: '95%', marginLeft: '5px', marginRight: '5px', paddingBottom: '10px', ':hover': { boxShadow: 20 } }}>
                        <img
                            src={`https://seeklogo.com/images/P/postman-logo-0087CA0D15-seeklogo.com.png`}
                            srcSet={`https://seeklogo.com/images/P/postman-logo-0087CA0D15-seeklogo.com.png`}
                            alt={"Postman"}
                            loading="lazy"
                            style={CardImage}
                        />
                        <CardHeader title="Our API" subheader="This links to the Postman documentation of our API, which is reachable at api.tuitionball.me" />
                    </Card>
                </Link>
            </Box>

            <Box sx={{ display: 'flex', justifyContent: 'center', maxWidth: '100%', marginLeft: "16px", marginRight: "16px"}}>
                <Typography variant="h4"><b>Tools Used To Make Our Site</b></Typography>
            </Box>

            <Box sx={{
                display: 'flex',
                flexWrap: 'wrap',
                flexDirection: 'row',
                justifyContent: 'center',
                maxWidth: '100%',
                marginLeft: "16px",
                marginRight: "16px",
            }}>
                {toolsUsed.map((tool: Tool) => (
                    <Link style={{ textDecoration: 'none' }} href={tool.link} target="_blank" rel="noopener noreferrer">
                        <Card key={tool.title}
                            sx={{ width: '300px', height: '95%', marginLeft: '5px', marginRight: '5px', paddingBottom: '5px', ':hover': { boxShadow: 20 } }}>
                            <img
                                src={`${tool.img}`}
                                srcSet={`${tool.img}`}
                                alt={tool.title}
                                loading="lazy"
                                style={CardImage}
                            />
                            <CardHeader title={tool.title} subheader={tool.usage} />
                        </Card>
                    </Link>
                ))}
            </Box>

            <Box sx={{ display: 'flex', justifyContent: 'center', maxWidth: '100%', marginLeft: "16px", marginRight: "16px"}}>
                <Typography variant="h4"><b>APIs Used To Scrape Data For Our Site</b></Typography>
            </Box>

            <Box sx={{
                display: 'flex',
                flexWrap: 'wrap',
                flexDirection: 'row',
                justifyContent: 'center',
                maxWidth: '100%',
                marginLeft: "16px",
                marginRight: "16px",
            }}>
                <Link style={{ textDecoration: 'none' }} href="https://api.collegefootballdata.com/api/docs/?url=/api-docs.json#/" target="_blank" rel="noopener noreferrer">
                    <Card key="Swagger API"
                        sx={{ width: '300px', height: '95%', marginLeft: '5px', marginRight: '5px', paddingBottom: '10px', ':hover': { boxShadow: 20 } }}>
                        <img
                            src={`https://avatars.githubusercontent.com/u/7658037?s=280&v=4`}
                            srcSet={`https://avatars.githubusercontent.com/u/7658037?s=280&v=4`}
                            alt="Swagger API"
                            loading="lazy"
                            style={CardImage}
                        />
                        <CardHeader title="Swagger" subheader="We use the Swagger API to gather all of our college football statistics for players and schools." />
                    </Card>
                </Link>

                <Link style={{ textDecoration: 'none' }} href="https://www.mediawiki.org/wiki/API:Main_page" target="_blank" rel="noopener noreferrer">
                    <Card key={"MediaWiki"}
                        sx={{ width: '300px', height: '95%', marginLeft: '5px', marginRight: '5px', paddingBottom: '10px', ':hover': { boxShadow: 20 } }}>
                        <img
                            src={`https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/MediaWiki-2020-logo.svg/1200px-MediaWiki-2020-logo.svg.png`}
                            srcSet={`https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/MediaWiki-2020-logo.svg/1200px-MediaWiki-2020-logo.svg.png`}
                            alt={"MediaWiki"}
                            loading="lazy"
                            style={CardImage}
                        />
                        <CardHeader title="MediaWiki" subheader="We use MediaWiki to scrape Wikipedia pages. Specifically, we got most of our images with this API as well as salary data for coaches." />
                    </Card>
                </Link>

                <Link style={{ textDecoration: 'none' }} href="https://collegescorecard.ed.gov/data/documentation/" target="_blank" rel="noopener noreferrer">
                    <Card key={"College Scorecard"}
                        sx={{ width: '300px', height: '95%', marginLeft: '5px', marginRight: '5px', paddingBottom: '10px', ':hover': { boxShadow: 20 } }}>
                        <img
                            src={`https://collegescorecard.ed.gov/img/US-DeptOfEducation-Seal.png`}
                            srcSet={`https://collegescorecard.ed.gov/img/US-DeptOfEducation-Seal.png`}
                            alt={"College Scorecard"}
                            loading="lazy"
                            style={CardImage}
                        />
                        <CardHeader title="College Scorecard" subheader="We use the College Scorecard API to get the data we have about each school, to include size and tuition." />
                    </Card>
                </Link>
            </Box>
        </Stack>
    )
}