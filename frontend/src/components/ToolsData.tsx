interface Tool {
    title: string;
    img: string;
    usage: string;
    link: string;
}

const toolsUsed: Tool[] = [
    {
        title: "Docker",
        img: "https://ms-azuretools.gallerycdn.vsassets.io/extensions/ms-azuretools/vscode-docker/1.20.0/1645549725810/Microsoft.VisualStudio.Services.Icons.Default",
        usage: "Docker is a platform which allows for the containerization of applications. It allows us to easily package our application and deploy it on any machine that has Docker installed.",
        link: "https://www.docker.com/",
    },
    {
        title: "Docker-Compose",
        img: "https://miro.medium.com/max/770/1*YE-fApWn9PXN9B3k7DY4FA.png",
        usage: "Docker-Compose is a tool used for defining and running applications which require multiple containers. We use Docker-Compose to define the resources our containers require—such as volumes, exposed ports, and environment variables—and seemlessly launch our application.",
        link: "https://docs.docker.com/compose/",
    },
    {
        title: "React",
        img: "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1200px-React-icon.svg.png",
        usage: "React is a frontend JavaScript library which allows for the development of web applications with interactive UIs. We use React as the frontend component in our application.",
        link: "https://reactjs.org/",
    },
    {
        title: "Material-UI",
        img: "https://v4.mui.com/static/logo.png",
        usage: "Material-UI is a library of React components which provide a framework for designing a web application. We use Material-UI for our designs and styling throughout our application.",
        link: "https://mui.com/",
    },
    {
        title: "Postman",
        img: "https://seeklogo.com/images/P/postman-logo-0087CA0D15-seeklogo.com.png",
        usage: "Postman is used to design our API and present it with examples. It is also used to test our API to ensure accurate results in our website.",
        link: "https://www.postman.com/company/about-postman/",
    },
    {
        title: "GitLab",
        img: "https://about.gitlab.com/images/press/logo/jpg/gitlab-logo-gray-stacked-rgb.jpg",
        usage: "GitLab is a DevOps platform for source code management and CI/CD. We use GitLab to manage all of our source code and for our CI/CD pipeline.",
        link: "https://about.gitlab.com/company/",
    },
    {
        title: "AWS Amplify",
        img: "https://seeklogo.com/images/A/aws-amplify-logo-D68DDB5AB1-seeklogo.com.png",
        usage: "AWS Amplify is used to host our website and make it accessible outside of our local hosting instances. It also allows for continuous integration with our repository so that changes in our code is reflected quickly.",
        link: "https://aws.amazon.com/amplify/",
    },
    {
        title: "AWS Elastic Beanstalk",
        img: "https://miro.medium.com/max/402/1*zOPbEexteQJIN3TMVTr-Eg.png",
        usage: "Elastic Beanstalk is a service offered by AWS that allows us to host our API publicly via a docker container.",
        link: "https://aws.amazon.com/elasticbeanstalk/",
    },
    {
        title: "Flask",
        img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQl7BEwqp9RfFRZt_guSqMHxWC2zFqrI8-rQ&usqp=CAU",
        usage: "Flask is a web framework written in the Python programming language. We use Flask to create our API, which our frontend calls to populate the model and instance pages.",
        link: "https://flask.palletsprojects.com/en/2.1.x/",
    },
    {
        title: "PostgreSQL",
        img: "https://upload.wikimedia.org/wikipedia/commons/2/29/Postgresql_elephant.svg",
        usage: "PostgreSQL is a free and open-source relational database management system. We use PostgreSQL to store all of the data on this site.",
        link: "https://www.postgresql.org/",
    },
    {
        title: "pgAdmin",
        img: "https://www.pgadmin.org/static/docs/pgadmin4-dev/docs/en_US/_build/html/_images/logo-right-128.png",
        usage: "pgAdmin is used to interact with our database to spot check data and confirm that all our backend data was populated correctly",
        link: "https://www.pgadmin.org/",
    },
    {
        title: "Selenium",
        img: "https://selenium-python.readthedocs.io/_static/logo.png",
        usage: "Selenium is a python library that allows for easy web scraping and GUI testing. We used this tool to scrape player images and perform GUI tests.",
        link: "https://www.selenium.dev/",
    },
    {
        title: "Jest",
        img: "https://miro.medium.com/max/600/1*i37IyHf6vnhqWIA9osxU3w.png",
        usage: "Jest is a JavaScript testing framework designed to ensure correctness of any JavaScript codebase. We use Jest to test the correctness of our frontend.",
        link: "https://jestjs.io/",
    }
];

export type { Tool };
export { toolsUsed };

