import React, {useState, useEffect} from 'react';
import {Box, Stack, Typography, Table, TableBody, TableCell, TableHead, TableRow, Button, InputLabel, Grid, Select, MenuItem, FormControl, TextField} from '@mui/material';
import {useNavigate} from 'react-router-dom';
import {getTuitionBallApi} from './APIServices'

// return type for each school returned by the API call
type schoolJSON = {
  id: string,
  school: String,
  size: String,
  tuition: String,
  type: String,
  conference: String,
  logo: string
}


function Schools(props: any) {
  const navigate = useNavigate();
  // get search/sort/filter parameters from url
  const params = new URLSearchParams(window.location.search);

  // loads school instance page when clicked on in the table of schools
  function handleClick(id: any){
    navigate("/SchoolsInstance/?id=" + id);
    window.location.reload();
  }
  
  // set default page parameter to 1
  if (!params.has('page')) {
    params.set('page','1');
  }

  const [searchParam, setSearchParam] = useState("");

  // this function handles the button to go back one page in the schools table
  // and checks if already on first page
  function handleBackward() {
    let page = Number(params.get('page'));
    if(page !== 1){
      let newPage = page-1;
      // set new page value, add to url, and reload page
      params.set('page', String(newPage));
      navigate("/schools/?" + params.toString());
      window.location.reload();
    }
  }

  // this function handles the button to go forward one page in the schools table
  // and checks if already on last page
  function handleForward(){
    let page = Number(params.get('page'));
    if(page !== resData.num_pages){
      let newPage = page+1;
      // set new page value, add to url, and reload page
      params.set("page", String(newPage));
      navigate("/schools/?" + params.toString());
      window.location.reload();
    }
  }

  // this function handles the button to go to the first page in the schools table
  function handleFirst() {
    if (Number(params.get('page')) !== 1) {
      // set new page value, add to url, and reload page
      params.set('page', '1');
      navigate("/schools/?" + params.toString());
      window.location.reload();
    }
  }

  // this function handles the button to go to the last page in the schools table
  function handleLast() {
    if(resData.num_pages !== 0 && Number(params.get('page')) !== resData.num_pages) {
      // set new page value, add to url, and reload page
      params.set('page', String(resData.num_pages));
      navigate("/schools/?" + params.toString());
      window.location.reload();
    }
  }

  // this function handles the sort buttons by adding the parameter to the url
  function handleSort(key: string) {
    if (params.has('sort')) {
      params.set('sort', key.replace(' ', '%20'));
    }
    else {
      params.append("sort", key.replace(" ", "%20"));
    }
    // set new sort param, add to url, and reload page
    params.set('page', '1');
    navigate("/schools/?" + params.toString());
    window.location.reload();
  }

  // this function handles the filter button by adding the parameter to the url
  function handleFilter(key: string, value: string) {
    if (params.has(key)) {
      params.set(key, value);
    }
    else {
      params.append(key, value);
    }
    // set new sort param, add to url, and reload page
    params.set('page', '1');
    navigate("/schools/?" + params.toString());
    window.location.reload();
  }

  // this function clears the filters by removing the parameter from the url
  // and reloading the page with a fresh set of results
  function clearFilter(key: string) {
    params.delete(key);
    params.set('page', '1');
    navigate("/schools/?" + params.toString());
    window.location.reload();
  }

  // this function is used to handle text typed in the search bar
  function handleChange(e: any) {
    setSearchParam(e.target.value);
  }

  // this function clears any current filtering/sorting parameters from the url and
  // adds the search parameter that is typed in once the enter key is hit and then
  // reloads the page with the searched results
  function handleSearch(e: any) {
    if (e.key === 'Enter') {
      params.delete('sort');
      params.delete('type');
      params.delete('conference');
      if(params.has('search')) {
        params.set('search', String(searchParam));
      }
      else {
        params.append('search', String(searchParam));
      }
      params.set('page', '1');
      navigate("/schools/?" + params.toString());
      window.location.reload();
    }
  
  }

  // this function searches through the text parameter and highlights the section
  // that equals the highlight parameter while leaving the rest of the text as is
  const Highlighted = ({ text = "", highlight = "" }) => {
    if (!highlight.trim()) {
      return <span>{text}</span>;
    }
    const regex = new RegExp(`(${highlight})`, "gi");
    const parts = text.split(regex);
  
    return (
      <span>
        {parts.filter(String).map((part, i) => {
          return regex.test(part) ? (
            <mark key={i}>{part}</mark>
          ) : (
            <span key={i}>{part}</span>
          );
        })}
      </span>
    );
  };

  const [schoolData, setData] = useState<schoolJSON[]>([]);
  const [resData, setResData] = useState({} as any);
  
  // call our APi to get schools results based on the url parameters as well as some
  // metadata about the results returned
  useEffect(() => {
    try {
      getTuitionBallApi("schools?sparse=true&" + params.toString())
      .then((res) => {
        setData(res.data);
        setResData(res);});
      console.log(schoolData)

    }
    catch (err) {
        console.error(err);
    }
  }, []);

  const dataFields = [{field: "school", title: "School"}, {field: "size", title: "Size"}, 
        {field: "tuition", title: "Tuition"}, {field: "type", title: "Ownership"}, {field: "conference", title: "Conference"}]


  return (
    <Stack spacing={4}>
      {/* Title */}
      <Box sx={{display: 'flex', justifyContent: 'center', padding: '5%'}}>
        <Typography variant="h3"><b>Schools</b></Typography>
      </Box>
      
      {/* Search bar */}
      <div className="SearchFunction">
        <Box sx={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
          <FormControl sx={{width: '75vw'}}>
            <TextField id="outlined-basic" label="Search" variant="outlined" value = {searchParam} onChange = {handleChange} onKeyDown = {handleSearch} />
          </FormControl>
        </Box>
          <Grid sx ={{ justifyContent: 'center', alignItems: 'center'}} container spacing={2}>
            <Grid item xs="auto">
              <h2 style = {{textAlign:'left'}}>Filter</h2>
            </Grid>
            {/* Sorting options */}
            <Grid item xs="auto">
              <Box sx={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                <FormControl sx={{ m: 1, minWidth: 100 }}>
                  <InputLabel id="demo-simple-select-label">Sort By</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select" 
                  >
                    <MenuItem onClick = {()=>handleSort("school")}>School A-Z</MenuItem>
                    <MenuItem onClick = {()=>handleSort("school-desc")}>School Z-A</MenuItem>
                    <MenuItem onClick = {()=>handleSort("size")}>Size Ascending</MenuItem>
                    <MenuItem onClick = {()=>handleSort("size-desc")}>Size Descending</MenuItem>
                    <MenuItem onClick = {()=>handleSort("tuition")}>Tuition Ascending</MenuItem>
                    <MenuItem onClick = {()=>handleSort("tuition-desc")}>Tuition Descending</MenuItem>
                    <MenuItem onClick = {()=>clearFilter("sort")}>Clear Filter</MenuItem>
                  </Select>
                </FormControl>
              </Box>
            </Grid>
            {/* Filter by ownership */}
            <Grid item xs="auto">
              <Box sx={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                <FormControl sx={{ m: 1, minWidth: 130 }}>
                  <InputLabel id="demo-simple-select-label">Ownership</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    label="Ownership"
                  >
                    <MenuItem onClick = {()=>handleFilter('type', 'public')}>Public</MenuItem>
                    <MenuItem onClick = {()=>handleFilter('type', 'private')}>Private</MenuItem>
                    <MenuItem onClick = {()=>clearFilter('type')}>Clear Filter</MenuItem>
                  </Select>
                </FormControl>
              </Box>
            </Grid>
            {/* Filter by conference */}
            <Grid item xs="auto">
              <Box sx={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                <FormControl sx={{ m: 1, minWidth: 130 }}>
                  <InputLabel id="demo-simple-select-label">Conference</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    label="Conference"
                  >
                    <MenuItem onClick = {()=>handleFilter('conference', 'Big 12')}>Big 12</MenuItem>
                    <MenuItem onClick = {()=>handleFilter('conference', 'SEC')}>SEC</MenuItem>
                    <MenuItem onClick = {()=>handleFilter('conference', 'Sun Belt')}>Sun Belt</MenuItem>
                    <MenuItem onClick = {()=>handleFilter('conference', 'Pac-12')}>Pac-12</MenuItem>
                    <MenuItem onClick = {()=>handleFilter('conference', 'Mid-American')}>Mid-American</MenuItem>
                    <MenuItem onClick = {()=>handleFilter('conference', 'Mountain West')}>Mountain West</MenuItem>
                    <MenuItem onClick = {()=>handleFilter('conference', 'ACC')}>ACC</MenuItem>
                    <MenuItem onClick = {()=>handleFilter('conference', 'FBS Independents')}>FBS Independents</MenuItem>
                    <MenuItem onClick = {()=>handleFilter('conference', 'American Athletic')}>American Athletic</MenuItem>
                    <MenuItem onClick = {()=>handleFilter('conference', 'Conference USA')}>Conference USA</MenuItem>
                    <MenuItem onClick = {()=>handleFilter('conference', 'Big Ten')}>Big Ten</MenuItem>
                    <MenuItem onClick = {()=>clearFilter('conference')}>Clear Filter</MenuItem>
                  </Select>
                  </FormControl>
              </Box>
            </Grid>
  
          </Grid>
      </div>
        {/* Table of API results for schools */}
        <Box sx={{ display: 'flex', justifyContent: 'center' , paddingBottom: '2%'}}>
          <Table aria-label="simple table" sx = {{width: '70%'}}>
              <TableHead>
                  <TableRow>
                      {dataFields.map((line: any) => <TableCell>{line.title}</TableCell>)}
                  </TableRow>
              </TableHead>
              <TableBody>
              {schoolData.map((val: schoolJSON, index: Number) => (
                  <TableRow
                  hover
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                      <TableCell onClick = {()=>handleClick(val.id)}><img alt="logo" src={val.logo} style={{height: '5%', width: '5%', margin: '0 auto'}}/>&ensp;{params.has('search') ? <Highlighted text={String(val.school)} highlight={String(params.get('search'))}/> : val.school}</TableCell>
                      <TableCell onClick = {()=>handleClick(val.id)}>{params.has('search') ? <Highlighted text={String(val.size)} highlight={String(params.get('search'))}/> : val.size}</TableCell>
                      <TableCell onClick = {()=>handleClick(val.id)}>{params.has('search') ? <Highlighted text={String(val.tuition)} highlight={String(params.get('search'))}/> : val.tuition}</TableCell>
                      <TableCell onClick = {()=>handleClick(val.id)}>{params.has('search') ? <Highlighted text={String(val.type)} highlight={String(params.get('search'))}/> : val.type}</TableCell>
                      <TableCell onClick = {()=>handleClick(val.id)}>{params.has('search') ? <Highlighted text={String(val.conference)} highlight={String(params.get('search'))}/> : val.conference}</TableCell>
                  </TableRow>
              ))}
              </TableBody>
          </Table>
        </Box>
        {/* Pagination buttons */}
        <Stack
          direction="row"
          justifyContent="center"
          alignItems="center"
          marginTop={4}
          spacing={4}
        >
          <Button onClick={handleFirst} variant="contained">{'<<'}</Button>
          <Button onClick={handleBackward} variant="contained">{'<'}</Button>
          <Typography gutterBottom variant="h5" component="div" fontWeight="bold">Page {params.get('page')}/{resData.num_pages}</Typography>
          <Button onClick={handleForward} variant="contained">{'>'}</Button>
          <Button onClick={handleLast} variant="contained">{'>>'}</Button>
        </Stack>
        {/* Metadata on number of results */}
        <Box sx={{ display: 'flex', justifyContent: 'center', paddingTop: '2%', paddingBottom: '5%' }}>
          <Typography variant="h5"><b>Total Schools: {resData.total_num_records}</b></Typography>
        </Box>


    </Stack>

  );
}

export default Schools;