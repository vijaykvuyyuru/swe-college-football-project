import { teamData } from './TeamData'

export const fetchRequest = async (url:string) => {
    try {
        // Fetch request and parse as JSON
        const response = await fetch(url)
        let data = await response.json()

        // Extract the url of the response's "next" relational Link header
        let next_page
        let link = response.headers.get("link")
        
        let re = /<([^>]+)>; rel="next"/        // RegEx to look for
        let match : RegExpExecArray | null
        if (link && (match = re.exec(link)) != null) {
            next_page = match[1]
        }

        // If another page exists, merge its output into the array recursively
        if (next_page) {
            data = data.concat(await fetchRequest(next_page))
        }
        return data
    } catch (err) {
        return console.error(err)
    }
}

export const getGitlabStats = async () => { 
    const commits = await fetchRequest('https://gitlab.com/api/v4/projects/33829565/repository/commits');

    teamData.forEach((member) => {
        member.NumCommits = 0
        member.NumIssues = 0
    })

    for (const commit in commits) {
        teamData.forEach((member) => {
            if (commits[commit].committer_email === member.Email) {
                member.NumCommits++
            }
        }) 
    }

    const issues = await fetchRequest('https://gitlab.com/api/v4/projects/33829565/issues');

    for (const issue in issues) {
        teamData.forEach((member) => {
            if (issues[issue].closed_by) {
                if (issues[issue].closed_by.username === member.Username) {
                    member.NumIssues++
                }
            }
        }) 
    }

    return teamData
}

export const getTuitionBallApi = async (path : string) => {
    let url : string = "https://api.tuitionball.me/"
    if (process.env.REACT_APP_USE_LOCAL_API) {
        url = "http://localhost:80/"
    }
    url += path
    const response = await fetchRequest(url)
    console.log(response)
    return response
}