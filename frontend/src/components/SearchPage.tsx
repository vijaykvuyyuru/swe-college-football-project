import React, {useState, useEffect} from 'react';
import {Box, Stack, Typography, Table, TableBody, TableCell, TableHead, TableRow, FormControl, TextField} from '@mui/material';
import {useNavigate} from 'react-router-dom';
import {getTuitionBallApi} from './APIServices';

// return type for schools returned by API
type schoolJSON = {
    id: string,
    school: string,
    size: string,
    tuition: string,
    type: string,
    conference: string,
    logo: string
}

// return type for players returned by API
type playerJSON = {
    id: string,
    first_name: string,
    last_name: string,
    school: string,
    position: string,
    year: string
}

// return type for coaches returned by API
type coachJSON = {
    id: string,
    first_name: string,
    last_name: string,
    salary: string,
    Current_school: string,
    hire_date: string,
    image: string,
    overall_wins: string,
    overall_losses: string,
    current_wins: string,
    current_losses: string,
    school_id: string
}

function SearchPage(props: any) {
    const navigate = useNavigate();
    //get search parameters from url
    const params = new URLSearchParams(window.location.search);

    const [searchParam, setSearchParam] = useState("");

    // this function is used to handle text typed in the search bar
    function handleChange(e: any) {
        setSearchParam(e.target.value);
    }
    
    // function to set url parameters to text entered in search bar when
    // enter is pressed
    function handleSearch(e: any) {
        if (e.key === 'Enter') {
            navigate("/SearchPage/?search=" + String(searchParam));
            window.location.reload();
        }
    }

    // function to navigate to school/player/coach instance when clicked on 
    // in search results tables
    function handleClick(instance: string, id: any){
        navigate("/" + instance + "/?id=" + id);
        window.location.reload();
    }

    const [data, setData] = useState({} as any);
    const [schoolData, setSchoolData] = useState<schoolJSON[]>([]);
    const [playerData, setPlayerData] = useState<playerJSON[]>([]);
    const [coachData, setCoachData] = useState<coachJSON[]>([]);

    // call API with search parameter
    useEffect(() => {
        try {
          getTuitionBallApi("search/" + String(params.get('search')))
          .then((res) => {
            setData(res);
            setSchoolData(res.schools);
            setPlayerData(res.players);
            setCoachData(res.coaches);  });
        }
        catch (err) {
            console.error(err);
        }
      }, []);

      const dataFields = [{field: "school", title: "School"}, {field: "size", title: "Size"}, 
      {field: "tuition", title: "Tuition"}, {field: "type", title: "Ownership"}, {field: "conference", title: "Conference"}]

      const dataFields2 = [ {field: "last_name", title: "Name"}, 
      {field: "school", title: "School"}, {field: "position", title: "Position"}, {field: "year", title: "Year"}]

      const dataFields3 = [ {field: "last_name", title: "Name"}, 
      {field: "school", title: "School"}, {field: "salary", title: "Salary"}, {field: "record", title: "Overall Record"}]

      // this function searches through the text parameter and highlights the section
      // that equals the highlight parameter while leaving the rest of the text as is
      const Highlighted = ({ text = "", highlight = "" }) => {
        if (!highlight.trim()) {
          return <span>{text}</span>;
        }
        const regex = new RegExp(`(${highlight})`, "gi");
        const parts = text.split(regex);
      
        return (
          <span>
            {parts.filter(String).map((part, i) => {
              return regex.test(part) ? (
                <mark key={i}>{part}</mark>
              ) : (
                <span key={i}>{part}</span>
              );
            })}
          </span>
        );
      };

      
      let results = (<Typography variant="h3" component="div" fontWeight="bold" sx={{display:"flex", justifyContent:"center", marginTop:"50px"}}> LOADING... </Typography>);
      if (data.schools) {
          results = (
              <Stack spacing={4}>
                {/* School results */}
                <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                    <Typography variant="h4" sx={{ paddingLeft: '16px'}}><b>School results</b></Typography>
                </Box>
                <Box sx={{ display: 'flex', justifyContent: 'center' , paddingBottom: '2%'}}>
                    {schoolData.length === 0 ? <Typography variant="body1" sx = {{paddingLeft: '16px'}}><b>No Schools Found</b></Typography> :
                    <Table aria-label="simple table" sx = {{width: '70%'}}>
                        <TableHead>
                            <TableRow>
                                {dataFields.map((line: any) => <TableCell>{line.title}</TableCell>)}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                        {schoolData.map((val: schoolJSON, index: Number) => (
                            <TableRow
                            hover
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell onClick = {()=>handleClick('SchoolsInstance', val.id)}><img alt="logo" src={val.logo} style={{height: '5%', width: '5%', margin: '0 auto'}}/>&ensp;<Highlighted text={String(val.school)} highlight={String(params.get('search'))}/></TableCell>
                                <TableCell onClick = {()=>handleClick('SchoolsInstance', val.id)}><Highlighted text={String(val.size)} highlight={String(params.get('search'))}/></TableCell>
                                <TableCell onClick = {()=>handleClick('SchoolsInstance', val.id)}><Highlighted text={String(val.tuition)} highlight={String(params.get('search'))}/></TableCell>
                                <TableCell onClick = {()=>handleClick('SchoolsInstance', val.id)}><Highlighted text={String(val.type)} highlight={String(params.get('search'))}/></TableCell>
                                <TableCell onClick = {()=>handleClick('SchoolsInstance', val.id)}><Highlighted text={String(val.conference)} highlight={String(params.get('search'))}/></TableCell>
                            </TableRow>
                        ))}
                        </TableBody>
                    </Table>
                    }
                </Box>

                {/* Player results */}
                <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                    <Typography variant="h4" sx={{ paddingLeft: '16px'}}><b>Player results</b></Typography>
                </Box>
                <Box sx={{ display: 'flex', justifyContent: 'center' , paddingBottom: '2%'}}>
                    {playerData.length === 0 ? <Typography variant="body1" sx = {{paddingLeft: '16px'}}><b>No Players Found</b></Typography> :
                    <Table aria-label="simple table" sx = {{width: '70%'}}>
                    <TableHead>
                        <TableRow>
                            {dataFields2.map((line: any) => <TableCell>{line.title}</TableCell>)}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    {playerData.map((val: playerJSON, index: Number) => (
                        <TableRow
                        hover
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                            <TableCell onClick = {()=>handleClick('PlayersInstance', val.id)}><Highlighted text={String(val.first_name + ' ' + val.last_name)} highlight={String(params.get('search'))}/></TableCell>
                            <TableCell onClick = {()=>handleClick('PlayersInstance', val.id)}><Highlighted text={String(val.school)} highlight={String(params.get('search'))}/></TableCell>
                            <TableCell onClick = {()=>handleClick('PlayersInstance', val.id)}><Highlighted text={String(val.position)} highlight={String(params.get('search'))}/></TableCell>
                            <TableCell onClick = {()=>handleClick('PlayersInstance', val.id)}><Highlighted text={String(val.year)} highlight={String(params.get('search'))}/></TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
                    }
                </Box>

                {/* Coach results */}
                <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                    <Typography variant="h4" sx={{ paddingLeft: '16px'}}><b>Coach results</b></Typography>
                </Box>
                <Box sx={{ display: 'flex', justifyContent: 'center' , paddingBottom: '5%'}}>
                    {coachData.length === 0 ? <Typography variant="body1" sx = {{paddingLeft: '16px', paddingBottom: '5%'}}><b>No Coaches Found</b></Typography> :
                    <Table aria-label="simple table" sx = {{width: '70%'}}>
                        <TableHead>
                            <TableRow>
                                {dataFields3.map((line: any) => <TableCell>{line.title}</TableCell>)}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                        {coachData.map((val: coachJSON, index: Number) => (
                            <TableRow
                            hover
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell onClick = {()=>handleClick('CoachesInstance', val.id)}><Highlighted text={String(val.first_name + ' ' + val.last_name)} highlight={String(params.get('search'))}/></TableCell>
                                <TableCell onClick = {()=>handleClick('CoachesInstance', val.id)}><Highlighted text={String(val.Current_school)} highlight={String(params.get('search'))}/></TableCell>
                                <TableCell onClick = {()=>handleClick('CoachesInstance', val.id)}><Highlighted text={String(val.salary)} highlight={String(params.get('search'))}/></TableCell>
                                <TableCell onClick = {()=>handleClick('CoachesInstance', val.id)}><Highlighted text={String(val.overall_wins +  ' - ' + val.overall_losses)} highlight={String(params.get('search'))}/></TableCell>
                            </TableRow>
                        ))}
                        </TableBody>
                    </Table>
                    }
                </Box>
            </Stack>
          );
      }
    return (
        <Stack spacing={4}>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h3" sx={{ paddingLeft: '16px', paddingTop: '3%' }}><b>Search results for '{params.get('search')}'</b></Typography>
            </Box>

            {/* Search Bar */}
            <Box sx={{display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '16px 0px 32px 0px'}}>
            <FormControl sx={{width: '75vw'}}>
                <TextField id="outlined-basic" label="Search" variant="outlined" value = {searchParam} onChange = {handleChange} onKeyDown = {handleSearch} />
            </FormControl>
            </Box>

            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                {results}
            </Box>
            

        </Stack>
    );
}

export default SearchPage;