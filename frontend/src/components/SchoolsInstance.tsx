import React, { useEffect, useState } from 'react';
import {Box, Stack, Typography, Table, TableBody, TableCell, TableHead, TableRow} from '@mui/material';
import {useNavigate} from 'react-router-dom';
import {getTuitionBallApi} from './APIServices';

// return type for API call for coach of this school instance
type coachJSON = {
    id: string,
    first_name: string,
    last_name: string,
    salary: String,
    Current_school: String,
    hire_date: String
  };

// return type for API call for players of this school instance
type playerJSON = {
    id: string,
    school: String,
    first_name: String,
    last_name: String,
    position: String,
    year: String
  };


function SchoolsInstance(props: any) {

    const navigate = useNavigate();

    // get search parameter from url
    const params = new URLSearchParams(window.location.search);
    let id:any;
    if(params.has('id')){
      id =Number(params.get('id'));
    }
    else
      id = 1;
    const [data, setData] = useState({} as any);

    // call API to get school results for this specific id from url parameter
    useEffect(() => {
        const getData = async () => {
            await getTuitionBallApi("schools?id=" + id).then(res=>setData(res));
        }
        getData();
    }, [id]);
    
    // call API to get player results for this school
    const [playerData, setPlayerData] = useState<playerJSON[]>([]);
        useEffect(() => {
            const getPlayers = async () => {
                let url = "players?team=" + data.school + "&sparse=true";
                url = url.replaceAll(" ","%20");
                await getTuitionBallApi(url).then(res=>setPlayerData(res.data));
            }
            getPlayers();
        }, [data]);

    // call API to get coach results for this school
    const [coachData, setCoachData] = useState<coachJSON[]>([])
        useEffect(() => {
            const getCoaches = async () => {
                let url = "coaches?team=" + data.school + "&sparse=true";
                url = url.replaceAll(" ","%20");
                try {
                    await getTuitionBallApi(url).then(res=>setCoachData(res.data));
                }
                catch (err) {
                    console.error(err);
                }
            }
            getCoaches();
        }, [data]);
    
    const playerFields = [{field: "first_name", title: "First Name"}, {field: "last_name", title: "Last Name"}, 
        {field: "position", title: "Position"}, {field: "year", title: "Year"}]

    // function to navigate to player clicked on in players table
    function handlePlayerClick(id:any){
      navigate("/PlayersInstance/?id=" + id);
    }

    // function to navigate to coach when clicked on 
    function handleCoachClick(id: any){
      navigate("/CoachesInstance/?id=" + id);
      window.location.reload();
    }

    function createText(text: string) {
        return {text};
    }

    // parse drafted players list returned by API to get individual players to
    // be displayed in tabular form
    function simpleParser(listOfPlayers: string) {
        const res = [];
        const arr = listOfPlayers.split(',');
        for (let i = 0; i < arr.length; i++) {
            res.push(createText(arr[i]));
        }
        return res;
    }

    const rows = simpleParser(String(data.nfl_players));


    let display = (<Typography variant="h3" component="div" fontWeight="bold" sx={{display:"flex", justifyContent:"center", marginTop:"50px"}}> LOADING... </Typography>);
    if(data.school) {
        display = (
            <Stack spacing={4}>
            {/* School, team mascot, logo */}
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h2" sx={{paddingTop: '5%' }}><b>{data.school}</b></Typography>
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h3"><b>{data.mascot}</b></Typography>
            </Box>
            <img src={data.logo} alt={data.mascot} style={{display: 'block', height: '30%', width: '30%', margin: '0 auto'}}/>
            
            {/* School info */}
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h4"><b>School Information</b></Typography>
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h5"><b>{data.size} Students</b></Typography>
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h5"><b>{"$" + data.tuition} Average Yearly Tuition</b></Typography>
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h5" sx={{paddingBottom: '5%'}}><b>{data.conference} Conference</b></Typography>
            </Box>
            
            {/* Stadium name and map */}
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h5"><b>{data.location}</b></Typography>
            </Box>

            <Box sx={{ display: 'flex', justifyContent: 'center', paddingBottom: '8%' }}>
            <iframe src={data.location ? "https://www.google.com/maps/embed/v1/place?key=AIzaSyDsRohslF-6LMq6lk-Nwqanwt1b8vf2ZK8&q=" + data.location.replace(" ", "+").replace("-", "+").replace('&', '') + data.school.replace(" ", "+").replace('&', '') : undefined} style = {{width:'50%', height:'80vh', border:'0'}} title="map"></iframe>
            </Box>

            {/* Team statistics */}
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h4"><b>2021 Team Statistics</b></Typography>
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h5"><b>Record: {data.wins + "-" + data.losses}</b></Typography>
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h5"><b>Total Passing Yards: {data.passing_yards}</b></Typography>
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'center'}}>
                <Typography variant="h5"><b>Recruiting Class Ranking: {data.recruiting_class_rank}</b></Typography>
            </Box>


            {/* Coach */}
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h5" sx={{ paddingTop: '5%'}}><b>Coach</b></Typography>
            </Box>

            <Box sx={{ display: 'flex', justifyContent: 'center' , paddingBottom: '5%'}}>
                <Table aria-label="simple table" sx = {{width: '50%'}}>
                    <TableHead>
                        <TableRow>
                            <TableCell sx={{textAlign: 'center'}}>Name</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    {coachData.map((val: coachJSON, index: any) => (
                        <TableRow
                        hover
                        sx={{ '&:last-child td, &:last-child th': { border: 0 }}}
                        >
                            <TableCell onClick={()=>handleCoachClick(val.id)} sx={{textAlign: 'center'}}>{val.first_name + ' ' + val.last_name}</TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
            </Box>

            {/* Player */}
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h5" sx={{paddingBottom: '1%'}}><b>Current Players</b></Typography>
            </Box>

            <Box sx={{ display: 'flex', justifyContent: 'center' , paddingBottom: '5%'}}>
                <Table aria-label="simple table" sx = {{width: '50%'}}>
                    <TableHead>
                        <TableRow>
                            {playerFields.map((line: any) => <TableCell>{line.title}</TableCell>)}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    {playerData.map((val: playerJSON, index: any) => (
                        <TableRow
                        hover
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                            <TableCell onClick={()=>handlePlayerClick(val.id)}>{val.first_name}</TableCell>
                            <TableCell onClick={()=>handlePlayerClick(val.id)}>{val.last_name}</TableCell>
                            <TableCell onClick={()=>handlePlayerClick(val.id)}>{val.position}</TableCell>
                            <TableCell onClick={()=>handlePlayerClick(val.id)}>{val.year}</TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
            </Box>

            {/* Drafted Players table */}
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h5"><b>Drafted Players</b></Typography>
            </Box>

            <Box sx={{ display: 'flex', justifyContent: 'center' , paddingBottom: '5%'}}>
            <Table aria-label="simple table" sx = {{width: '50%'}}>
                <TableHead>
                <TableRow>
                    <TableCell sx={{textAlign: 'center'}}>Player &emsp; &ensp; Position &emsp; Pick &emsp; Team</TableCell>
                </TableRow>
                </TableHead>
                <TableBody>
                {rows.map((row) => (
                    <TableRow
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                        <TableCell sx={{textAlign: 'center'}}>{row.text}</TableCell>
                    </TableRow>
                ))}
                </TableBody>
            </Table>
            </Box>
                
        </Stack>
        );
    }
    return display;

};

export default SchoolsInstance;