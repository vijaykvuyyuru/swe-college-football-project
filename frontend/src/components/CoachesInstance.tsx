import React, { useState, useEffect} from 'react';
import {Stack, Container, Card, Divider, Box, Table, TableRow, TableCell, TableHead, TableBody, Grid} from '@mui/material';
import Typography from '@mui/material/Typography';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import { CardActionArea } from '@mui/material';
import {useNavigate} from 'react-router-dom';
import {getTuitionBallApi} from './APIServices';


function CoachesInstance(props: any) {

  // Object used to navigate to different pages
  const navigation = useNavigate();

  // Object to help manipulate url parameters
  const params = new URLSearchParams(window.location.search);

  // Sets the coach id to get from the API
  let id:any;
  if(params.has('id')){
    id =Number(params.get('id'));
  }
  else
    id = 1;

  const [coachData, setCoachData] = useState({} as any);

  // On first render, gets the coach data from the API
  useEffect(() => {
      const getData = async () => {

              await getTuitionBallApi("coaches?id="+ id).then(res=>setCoachData(res));
      }
      getData();
  }, [id]);

  const [playersData, setPlayersData] = useState({} as any);

  // After coaches data is collected, gets the coach's players data from the API
  useEffect(() => {
      const getPlayers = async () => {
          let url = "players?team=" + coachData.Current_school;
          await getTuitionBallApi(url).then(res=>setPlayersData(res));
      }
      getPlayers();
  }, [coachData]);

  // Navigates to the player instance page with the provided id
  function handlePlayerClick(id:Number){
    navigation("/PlayersInstance?id=" + id);
  }

  // Navigates to the school instancee page with the provided id
  function handleSchoolClick(id: Number){
    navigation("/SchoolsInstance/?id=" + id);
    window.location.reload();
  }

  // Navigates to the coach instancee page with the provided id
  function handleCoachClick(id: Number){
    navigation("/CoachesInstance/?id=" + id);
    window.location.reload();
  }

  // Builds the player card grid
  let grid = (<Typography variant="h5" component="div" fontWeight="bold"> LOADING... </Typography>);
  if(playersData.data){
      grid = playersData.data.map((playerData : any) => (
      <Grid item xs={3} sx={{display:"flex", justifyContent:"center"}}>
      <Card sx={{ width: 280, ':hover': { boxShadow: 20 }, border:"solid lightgray 1px"}}>
        <CardActionArea onClick={()=>handlePlayerClick(playerData.id)}> 
          <CardMedia
            component="img"
            height="370"
            width="auto"
            image={playerData.picture}
            alt={playerData.first_name + " " + playerData.last_name}
          />
          <CardContent sx={{backgroundColor:"#FAF9F6", borderTop:"solid gray 2px"}}>
            <Typography gutterBottom variant="h5" component="div" fontWeight="bold">
              {playerData.first_name + " " + playerData.last_name}
            </Typography>
              <ul style={{ listStyleType:'none', margin:"0", padding:"0"}}>
                <li key={playerData.id} style={{color:'blue', textDecoration:'underline'}} onClick={()=> handleSchoolClick(playerData.school_id)}>School: {playerData.school}</li>
                <li key={-1} style={{color:'blue', textDecoration:'underline'}} onClick={()=> handleCoachClick(playerData.coach_id)}>Coach: {playerData.coach_first_name + " " + playerData.coach_last_name}</li>
                <li key={-2}>Position: {playerData.position}</li>
                <li key={-3}>Number: {playerData.jersey}</li>
                <li key={-4}>Class: {playerData.year}</li>
              </ul>
          </CardContent>
        </CardActionArea>
      </Card>
      </Grid>
     ));
  }

  // Displays loading if player data is not collected yet
  let display = (<Typography variant="h3" component="div" fontWeight="bold" sx={{display:"flex", justifyContent:"center", marginTop:"50px"}}> LOADING... </Typography>);
  
  // If coachData has been retrieved, display the page
  if(coachData.image){
    display = (
      <Stack
      direction="column"
      justifyContent="center"
      alignItems="center"
      spacing={2}
      marginTop={4}
    >
      <Container sx={{ width: '75%', marginBottom:'20px'}}>
        <Stack  
          direction="row"
          justifyContent="center"
          alignItems="Center"
          spacing={2}
          marginBottom={4}
          >
            {/* Coach Image  */}
            <Card sx={{maxWidth:300, minWidth: 300 }}>
              <CardMedia
                component="img"
                height="400"
                image={coachData.image}
                alt={coachData.first_name + " " + coachData.last_name}
              />
            </Card>

            <Container>

              {/* School Logo and Coach Name  */}
              <Typography variant="h1" sx={{display:'flex',fontSize:'50px', fontWeight:'bold', border: '3px solid black', justifyContent:'center'}}>{coachData.first_name + " " + coachData.last_name}&nbsp;<img height='50px' style={{marginTop:'5px'}} src={coachData.school_logo} alt="team logo"></img></Typography>
              
                <Stack
                  direction="row"
                  justifyContent="center"
                  alignItems="center"
                  spacing={2}
                >
                  {/* Coach Information  */}
                  <Container sx={{textAlign:'right', fontSize:'25px'}}>
                    <ul style={{ listStyleType:'none', width:'max-content'}}>
                      <li><b>School:</b></li>
                      <li><b>Overall Record:</b></li>
                      <li><b>Current Record:</b></li>
                      <li><b>Salary:</b></li>
                    </ul>
                  </Container>

                  <Container sx={{ textAlign:'left', fontSize:'25px'}}>
                    <ul style={{ listStyleType:'none', width:'max-content'}}>
                      <li style={{color:'blue', textDecoration:'underline', cursor:"pointer"}} onClick={()=> handleSchoolClick(coachData.school_id)}>{coachData.Current_school}</li>
                      <li> {coachData.overall_wins + "-" + coachData.overall_losses}&nbsp;</li>
                      <li> {coachData.current_wins + "-" + coachData.current_losses}&nbsp;</li>
                      <li> ${(coachData.salary === "Not found" ? "1500000" : coachData.salary)}&nbsp;</li>
                    </ul>
                  </Container>
                </Stack>
                
            </Container>
        </Stack>
        <Divider variant="middle" sx={{backgroundColor: 'black'}}/>
      </Container>

      <Typography sx={{display:'flex',fontSize:'30px', fontWeight:'bold', justifyContent:'center'}}>Notable Players</Typography>

      {/* Notable Players Grid  */}
      <Grid
                container
                spacing={2}
                display="flex"
                justifyContent="center"
                margin="auto"
                direction="row"
                alignItems="flex-start"
            >
        {grid}

      </Grid>
      
      <Container sx={{ width: '75%', marginBottom:'20px'}}>
        <Divider variant="middle" sx={{backgroundColor: 'black', marginTop:"30px"}}/>
      </Container>

      <Typography sx={{display:'flex',fontSize:'30px', fontWeight:'bold', justifyContent:'center'}}>Statistics</Typography>
      
      {/* Coach Statistics by Season  */}
      <Box sx={{ display: 'flex', justifyContent: 'center', border: '3px solid gray', borderRadius: '15px'}}>
          <Table aria-label="simple table" sx = {{width: '70%'}}>
            <TableHead>
              <TableRow>
                <TableCell align="left" sx={{fontWeight:"bold"}}>Season</TableCell>
                <TableCell align="left" sx={{fontWeight:"bold"}}>School</TableCell>
                <TableCell align="left" sx={{fontWeight:"bold"}}>Wins</TableCell>
                <TableCell align="left" sx={{fontWeight:"bold"}}>Losses</TableCell>

              </TableRow>
            </TableHead>
            <TableBody>
              { coachData.history ? coachData.history.map((stats : any) => (
                <TableRow
                  key={stats.season}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell component="th" scope="row" align="left">{stats.season}</TableCell>
                  <TableCell align="left">{stats.school}</TableCell>
                  <TableCell align="left">{stats.wins}</TableCell>
                  <TableCell align="left">{stats.losses}</TableCell>
                </TableRow>
              )) : <Typography sx={{display:'flex', justifyContent:'center'}}variant="h5" component="div" fontWeight="bold"> LOADING... </Typography>}
            </TableBody>
          </Table>
      </Box>
    </Stack>
    );
  }


  return display;
};

export default CoachesInstance;