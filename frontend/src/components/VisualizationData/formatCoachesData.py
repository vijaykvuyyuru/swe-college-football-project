import json
from tqdm import tqdm
import requests

all_coaches = []

url = 'https://api.tuitionball.me/coaches'
num_pages = requests.get(f"{url}?sparse=true").json()["num_pages"]
print(num_pages)

for i in tqdm(range(1, num_pages+1)):
    data = requests.get(f"{url}?page={i}")
    list_coaches = data.json()["data"]
    for coach in list_coaches:
        coach["color"] = requests.get(f"https://api.tuitionball.me/schools?id={coach['school_id']}").json()["color"]
        all_coaches.append(coach)


transformed_data = []

for coach in all_coaches:
    new_coach = {"name": f"{coach['first_name']} {coach['last_name']}","color": f"{coach['color']}", "data": []}
    for year in coach["history"]:
        cur_year = {"year": year["season"], "value": year["wins"]}
        new_coach["data"].append(cur_year)
    transformed_data.append(new_coach)

with open("coachesData.json", "w") as f:
    json.dump(transformed_data, f)
