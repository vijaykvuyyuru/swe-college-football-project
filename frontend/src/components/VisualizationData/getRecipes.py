import json
import requests
from tqdm import tqdm


subregions = ["Africa", "Asia", "Europe", "North America", "South America"]

url = 'https://api.feastforfriends.me/api/cultures?'
num_recipes = []

for subregion in tqdm(subregions):
    num_pages = requests.get(f"{url}subregion={subregion}").json()["num_pages"]
    recipes = 0
    for i in range(1, num_pages+1):
        request = requests.get(f"{url}subregion={subregion}&page_num={i}").json()
        for culture in request["data"]:
            recipes += culture["num_recipe_connections"]
    num_recipes.append({"subregion": subregion, "recipes": recipes})

print(num_recipes)
with open("recipeData.json", "w") as f:
    json.dump(num_recipes, f)
