import json
from tqdm import tqdm
import requests

all_coaches = []

url = 'https://api.tuitionball.me/players'
num_pages = requests.get(f"{url}?sparse=true").json()["num_pages"]
print(num_pages)

jersey_number_counts = {}
for i in range(1, 100):
    jersey_number_counts[i] = 0

for i in tqdm(range(1, num_pages+1)):
    data = requests.get(f"{url}?page={i}")
    list_players = data.json()["data"]
    for player in list_players:
        jersey_number = player["jersey"]
        if jersey_number is not None:
            jersey_number_counts[jersey_number] += 1

transformed_data = []

for jersey_number, frequency in jersey_number_counts.items():
    transformed = {"jersey_number": jersey_number, "frequency": frequency}
    transformed_data.append(transformed)

with open("playersData.json", "w") as f:
    json.dump(transformed_data, f)
