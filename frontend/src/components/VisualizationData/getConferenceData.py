import json
import requests
from tqdm import tqdm

valid_division = ["American Athletic", "ACC", "Big 12", "Conference USA", "FBS Independents", "Mid-American",
                  "Mountain West", "Pac-12", "SEC", "Sun Belt", "Big Ten"]
url = 'https://api.tuitionball.me/schools?'

num_pages = requests.get(url).json()["num_pages"]
conference_tuitions = []
for division in tqdm(valid_division):
    request = requests.get(f"{url}conference={division}").json()
    tuition = 0
    for school in request["data"]:
        tuition += school["tuition"] if school["tuition"] is not None else 0
    conference_tuitions.append({"name": division, "cost": tuition})

with open("conferenceData.json", "w") as f:
    json.dump(conference_tuitions, f)
