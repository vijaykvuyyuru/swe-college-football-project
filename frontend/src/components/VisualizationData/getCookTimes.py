import json
import requests
from tqdm import tqdm

def sortBy(val):
  return val["time"]

url = 'https://api.feastforfriends.me/api/recipes?'
cook_times = {}
num_pages = requests.get(f"{url}").json()["num_pages"]
for i in range(1, num_pages+1):
    request = requests.get(f"{url}&page_num={i}").json()
    for recipe in request["data"]:
      if(recipe["time"] in cook_times):
        cook_times[recipe["time"]] += 1
      else:
        cook_times[recipe["time"]] = 1

reformat = []
for cook_time in cook_times:
  reformat.append({"time": cook_time, "Number of Recipes": cook_times[cook_time], "key": 1})
reformat.sort(key=sortBy)
print(reformat)
with open("cookTimeData.json", "w") as f:
    json.dump(reformat, f)
