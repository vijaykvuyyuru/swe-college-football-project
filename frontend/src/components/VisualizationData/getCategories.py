import json
import requests
from tqdm import tqdm

def sortBy(val):
  return val["category"][1]

url = 'https://api.feastforfriends.me/api/recipes?'
categories = {}
num_pages = requests.get(f"{url}").json()["num_pages"]
for i in tqdm(range(1, num_pages+1)):
    request = requests.get(f"{url}&page_num={i}").json()
    for recipe in request["data"]:
      if(recipe["category"] in categories):
        categories[recipe["category"]] += 1
      else:
        categories[recipe["category"]] = 1

reformat = []
for category in categories:
  reformat.append({"category": category, "Number of Recipes": categories[category]})
reformat.sort(key=sortBy)
print(reformat)
with open("categoryData.json", "w") as f:
    json.dump(reformat, f)
