The Python files in this directory each correspond to one json file that contains the cached results of api calls and some post processing to save rendering on the client.

`formatCoachesData.py` calls https://api.tuitionball.me/coaches and creates `coachesData.json`

`getConferenceData.py` calls https://api.tuitionball.me/schools and creates `conferenceData.json`

`getPlayerData.py` calls https://api.tuitionball.me/players and creates `playersData.json`

`getRecipes.py` calls https://api.feastforfriends.me/api/cultures and creates `recipeData.json`

`getCookTimes.py` calls https://api.feastforfriends.me/api/recipes and creates `cookTimeData.json`

Re-running any of these files will generate new cached api data.