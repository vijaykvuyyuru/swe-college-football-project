import React, { useState, useEffect} from 'react';
import {Stack, Container, Card, CardMedia, Divider, Typography, Box, Table, TableRow, TableCell, TableHead, TableBody} from '@mui/material';
import {useNavigate} from 'react-router-dom';
import {getTuitionBallApi} from './APIServices';

function PlayersInstance(props: any) {

  // Object used to navigate to different pages
  const navigation= useNavigate();

  // Object to help manipulate url parameters
  const params = new URLSearchParams(window.location.search);

  // Sets the player id to get from the API
  let id:any;
  if(params.has('id')){
    id =Number(params.get('id'));
  }
  else
    id = 1;
  const [playerData, setPlayerData] = useState({} as any)

  // On first render, retrieves the player data from the API
  useEffect(() => {
          try {
              getTuitionBallApi("players?id="+ id).then(res=>setPlayerData(res));
          }
          catch (err) {
              console.error(err);
          }
  }, [id]);

  // Navigates to the school instance page with the provided id
  function handleSchoolClick(id: Number){
    navigation("/SchoolsInstance/?id=" + id);
    window.location.reload();
  }

  // Navigates to the coach instancee page with the provided id
  function handleCoachClick(id: Number){
    navigation("/CoachesInstance/?id=" + id);
    window.location.reload();
  }

  // Displays loading if player data is not collected yet
  let display = (<Typography variant="h3" component="div" fontWeight="bold" sx={{display:"flex", justifyContent:"center", marginTop:"50px"}}> LOADING... </Typography>);

  // If playerData has been retrieved, display the page
  if(playerData.first_name){
    display = (
      <Stack
        direction="column"
        justifyContent="center"
        alignItems="center"
        spacing={2}
        marginTop={4}
      >
        <Container sx={{ width: '75%', marginBottom:'20px'}}>
          <Stack  
            direction="row"
            justifyContent="center"
            alignItems="Center"
            spacing={2}
            marginBottom={4}
            >
              {/* Player Image */}
              <Card sx={{maxWidth:300, minWidth: 300 }}>
                <CardMedia
                  component="img"
                  height="400"
                  image={playerData.picture}
                  alt={playerData.first_name + " " + playerData.last_name}
                />
              </Card>

              <Container>

                {/* Player Name and School Logo */}
                <Typography variant="h1" sx={{display:'flex',fontSize:'50px', fontWeight:'bold', border: '3px solid black', justifyContent:'center'}}>{playerData.first_name + " " + playerData.last_name}&nbsp;<img height='50px' style={{marginTop:'5px'}} src={playerData.school_logo} alt="team logo"></img></Typography>

                  {/* Player Information */}
                  <Stack
                    direction="row"
                    justifyContent="center"
                    alignItems="center"
                    spacing={2}
                  >
                    <Container sx={{textAlign:'right', fontSize:'25px'}}>
                    <ul style={{ listStyleType:'none', width:'max-content'}}>
                        <li><b>School:</b></li>
                        <li><b>Coach:</b></li>
                        <li><b>Postion:</b></li>
                        <li><b>Number:</b></li>
                        <li><b>Class:</b></li>
                        <li><b>Height:</b></li>
                        <li><b>Weight:</b></li>
                        <li><b>Hometown:</b></li>
                      </ul>
                    </Container>

                    <Container sx={{ textAlign:'left', fontSize:'25px'}}>
                      <ul style={{ listStyleType:'none', width:'max-content'}}>
                        <li style={{color:'blue', textDecoration:'underline', cursor:"pointer"}} onClick={()=> handleSchoolClick(playerData.school_id)}>{playerData.school}</li>
                        <li style={{color:'blue', textDecoration:'underline', cursor:"pointer"}} onClick={()=> handleCoachClick(playerData.coach_id)}> {playerData.coach_first_name + " " + playerData.coach_last_name}&nbsp;</li>
                        <li> {playerData.position}&nbsp;</li>
                        <li> {playerData.jersey}&nbsp;</li>
                        <li> {playerData.year}&nbsp;</li>
                        <li> {Math.floor(playerData.height/12)}'{playerData.height%12}"&nbsp;</li>
                        <li> {playerData.weight} lbs&nbsp;</li>
                        <li> {playerData.home_city + ", " + playerData.home_state}&nbsp;</li>
                      </ul>
                    </Container>
                  </Stack>
              </Container>
          </Stack>
          <Divider variant="middle" sx={{backgroundColor: 'black'}}/>
        </Container>

        <Typography sx={{display:'flex',fontSize:'30px', fontWeight:'bold', justifyContent:'center'}}>Statistics</Typography>

        {/* Statistics Table */}
        <Box sx={{ display: 'flex', justifyContent: 'center', border: '3px solid gray', borderRadius: '15px'}}>
            <Table aria-label="simple table" sx = {{width: '70%'}}>
              <TableHead>
                <TableRow>
                  <TableCell align="left" sx={{fontWeight:"bold"}}>First Down</TableCell>
                  <TableCell align="left" sx={{fontWeight:"bold"}}>Second Down</TableCell>
                  <TableCell align="left" sx={{fontWeight:"bold"}}>Third Down</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                  <TableRow
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell align="left">{playerData.first_down}</TableCell>
                    <TableCell align="left">{playerData.second_down}</TableCell>
                    <TableCell align="left">{playerData.third_down}</TableCell>
                  </TableRow>
              </TableBody>
            </Table>
        </Box>
        
        <Container sx={{ width: '75%', marginBottom:'20px'}}>
          <Divider variant="middle" sx={{backgroundColor: 'black', marginTop:"30px"}}/>

          <Typography sx={{display:'flex',fontSize:'30px', fontWeight:'bold', justifyContent:'center', marginTop:"30px"}}>Map of Hometown</Typography>

          {/* Map of Hometown */}
          <Box sx={{ display: 'flex', justifyContent: 'center', paddingBottom: '10%' }}>
            <iframe src={playerData.home_city ? "https://www.google.com/maps/embed/v1/place?key=AIzaSyDsRohslF-6LMq6lk-Nwqanwt1b8vf2ZK8&q=" + playerData.home_city + "+" + playerData.home_state : undefined} style = {{width:'70%', height:'100vh', border:'0'}} title="map"></iframe>
          </Box>

        </Container>
      </Stack>
    );
  }
  return display;
};

export default PlayersInstance;