import {Box, Typography, Stack, Paper, FormControl, Card, CardHeader, CardActions, Button, CardMedia, TextField} from '@mui/material';
import { styled } from '@mui/system';
import { useNavigate } from 'react-router-dom'
import stadium from './media/stad4.jpg';
import React, {useState} from 'react';

const StyledImg = styled(Paper)({
    display: 'block',
    width: '100%',
    height: '325px',
    overflow: 'hidden',
    justifyContent: 'center',
  });  

export const Home = () => {
    const navigation = useNavigate();
    const [searchParam, setSearchParam] = useState("");

    // this function is used to handle text typed in the search bar
    function handleChange(e: any) {
        setSearchParam(e.target.value);
    }
    
    // function to navigate to search page and set url parameters to text entered 
    // in search bar when enter is pressed
    function handleSearch(e: any) {
        if (e.key === 'Enter') {
            navigation("/SearchPage/?search=" + String(searchParam));
            window.location.reload();
        }
    }

    return (
        <Stack spacing={4}>
            <Box sx={{ display: 'flex', justifyContent: 'center', boxShadow: 3 }}>
                <StyledImg>
                    <img src={stadium} alt="stadium" style={{
                        minHeight: '250px',
                        top: '100%'
                    }}/>
                </StyledImg>
            </Box>

            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h2" sx={{ paddingLeft: '16px' }}><b>Welcome to TuitionBall</b></Typography>
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography align='center' sx={{paddingLeft: '100px', paddingRight:'100px'}}>Football can be a massive part of campus culture depending on which university you attend. Do you want to see how your team stacks up  or football statistics and tuition? You've come to the right place! Search for players, schools, and coaches across the NCAA division 1.</Typography>
            </Box>
            <Box sx={{display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '16px 0px 32px 0px'}}>
                <FormControl sx={{width: '75vw'}}>
                    <TextField id="outlined-basic" label="Search" variant="outlined" value = {searchParam} onChange = {handleChange} onKeyDown = {handleSearch} />
                </FormControl>
            </Box>
            <Box sx={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginLeft: '64px',
            maxWidth: '100%',
            padding: '64px 64px',
            backgroundColor: '#C4A484',
            }}
            >
                <Card sx={{ width: '500px', marginRight: '32px' }}>
                    {/*Image from https://www.reddit.com/r/MapPorn/comments/17yl19/map_of_all_division_1a_fbs_college_football_teams/*/}
                    <CardMedia image={require("./media/schools.jpg")} title="Schools" sx={{
                        height: '300px'
                    }}/>
                    <CardHeader
                    title="Search Schools"
                    subheader="Find all the division 1 football schools!"
                    />
                    <CardActions>
                        <Button onClick={()=>navigation('/schools')}>
                            Search Schools
                        </Button>
                    </CardActions>
                </Card>
                <Card sx={{ width: '500px', marginRight: '32px' }}>
                    {/* Image from https://www.hookem.com/story/sports/football/2021/01/12/now-texas-sark-makes-first-remarks-head-coach/6647761002/*/}
                    <CardMedia image={require("./media/coach.jpg")} title="Coaches" sx={{
                        height: '300px'
                    }}/>
                    <CardHeader
                    title="Search Coaches"
                    subheader="Find all the coaches that drive these teams to wins and losses!"
                    />
                    <CardActions>
                        <Button onClick={()=>navigation('/coaches')}>
                            Search Coaches
                        </Button>
                    </CardActions>
                </Card>
                <Card sx={{ width: '500px', marginRight: '32px' }}>
                    {/* Image from https://www.usatoday.com/story/sports/ncaaf/2020/07/07/covid-19-college-football-parents-seek-hold-schools-accountable/5389812002/*/}
                    <CardMedia image={require("./media/players.jpg")} title="players" sx={{
                        height: '300px'
                    }}/>
                    <CardHeader
                    title="Search Players"
                    subheader="Find the people on the field, who might be your peers!"
                    />
                    <CardActions>
                        <Button onClick={()=>navigation('/players')}>
                            Search Players
                        </Button>
                    </CardActions>
                </Card>
            </Box>
        </Stack>
    )
}
