import {Box, Typography, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper} from '@mui/material';
import positions from './media/positions.png';

function createData(position: string, acronym: string, description: string){
    return {position, acronym, description};
}

const offensiveRows = [
    createData("Quarterback", "QB", "The quarterback is the leader of the offense. He starts every play with the ball and is responsible for either passing the ball to a wide receiver or handing it off to the running back according to the design of the play."),
    createData("Center", "C", "The center is responsible for starting the play by snapping the ball between his legs to the quarterback. He has the central position on the offensive line and blocks defensive lineman."),
    createData("Guard", "G", "There are two guards on the offensive line on either side of the center. They are responsible for both protecting the quarterback when he is passing and blocking defensive players on running plays."),
    createData("Tackle", "T", "There are two tackles on the offensive line positioned outside of the guards. Their main responsibility is to protect the quarterback by blocking the defensive ends."),
    createData("Wide Receiver", "WR", "There are typically two or three wide recievers in every play. They run routes and catch passes from the quarterback. Recievers are usually the fastest players on the team."),
    createData("Tight End", "TE", "There are typically one or two tight ends in every play. They either help the offensive line in blocking or run routes and catch passes like a receiver depending on the play being run."),
    createData("Running Back", "RB", "Running backs are usually lined up behind the quarterback and are handed the ball on running plays to try and avoid defenders to gain yards. They can also run routes like recievers or provide additional blocking for the quarterback depending on the play."),
];

//lol UT doesnt use these
const defensiveRows = [
    createData("Defensive end", "DE", "There are usually two defensive ends lined up on the ends of the defensive line. Their objective is to avoid being blocked by the offensive tackles and sack the quarterback behind the line of scrimmage."),
    createData("Defensive tackle", "DT", "Defensive tackles are lined up in the middle of the defensive line opposite of the center or guards. Their main objective is to stop anybody from running through the middle of the defense, and they also try to sack the quarterback on passing plays."),
    createData("Linebacker", "LB", "Linebackers are usually the leaders of the defense. They are responsible for tackling running backs who make it past the defensive line or covering the middle of the field on passing plays."),
    createData("Cornerback", "CB", "Cornerbacks are lined up opposite of wide receivers. They shadow the recievers and prevent them from catching passes or gaining any yards."),
    createData("Safety", "S", "The safety is also responble for preventing receivers from catching passes, but they usually do not have a specific wide receiver to cover. They typically cover a deep zone of the field to prevent long passes or touchdowns."),
];

export const TheBasics = () => {
    return (
        <Stack spacing={2}>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h2" sx={{ paddingLeft: '16px', paddingTop: '16px'}}><b>The Basics!</b></Typography>
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography align='center' sx={{paddingLeft: '100px', paddingRight:'100px'}}>Football is incredibly confusing. There are many positions on the field each with their own 2 letter acronym. What does each role do? What are some good or even average statistics for passing yards, rushing yards, and downs? This page will break down each role as well as some statistics to give context to other information that can be found on this site. The game is made up 2 teams where each team takes turns playing offense and defense. On offense, the objective is to march the ball down to field to either kick the ball through the goal posts or get the ball into the endzone to score points. The team on defense attempts to stop this. The offense must advance 10 yards every 4 plays or downs.</Typography>
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h4" sx={{ paddingLeft: '16px', paddingTop: '16px'}}><b>Positions</b></Typography>
            </Box>
            {/* Image pulled from https://genius.com/Sports-genius-american-football-positions-annotated*/}
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <img src={positions} alt="positions" style={{minHeight: '250px', top: '100%', paddingLeft: '16px'}}/>
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography align='left' sx={{paddingLeft: '100px', paddingRight:'100px'}}>There are many positions in football, each of which plays a unique role. They can generally be split into offense and defense. </Typography>
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography align='center' variant="h5" sx={{paddingLeft: '100px', paddingRight:'100px'}}>Offensive positions</Typography>
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHead>
                        <TableRow>
                            <TableCell>Position</TableCell>
                            <TableCell align="center">Acryonm</TableCell>
                            <TableCell align="left">Description</TableCell>
                        </TableRow>
                        </TableHead>
                        <TableBody>
                        {offensiveRows.map((row) => (
                            <TableRow
                            key={row.position}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                            <TableCell component="th" scope="row">
                                {row.position}
                            </TableCell>
                            <TableCell align="center">{row.acronym}</TableCell>
                            <TableCell align="left">{row.description}</TableCell>
                            </TableRow>
                        ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography align='center' variant="h5" sx={{paddingLeft: '100px', paddingRight:'100px'}}>Defensive positions</Typography>
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHead>
                        <TableRow>
                            <TableCell>Position</TableCell>
                            <TableCell align="center">Acryonm</TableCell>
                            <TableCell align="left">Description</TableCell>
                        </TableRow>
                        </TableHead>
                        <TableBody>
                        {defensiveRows.map((row) => (
                            <TableRow
                            key={row.position}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                            <TableCell component="th" scope="row">
                                {row.position}
                            </TableCell>
                            <TableCell align="center">{row.acronym}</TableCell>
                            <TableCell align="left">{row.description}</TableCell>
                            </TableRow>
                        ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'left' }}>
                <Typography align='left' sx={{paddingLeft: '100px', paddingRight:'100px'}}>Now for an explanation of the statistics. There are several statistics a player can have. Many of these statistics can be split by player or by team.
                    <ul>
                        <li>Passing yards is a measure of how many yards the offensive team gains after passing the ball. This is typically accredited to the passer (usually the quarterback). This statistic is cumulative. Typically over the course of a season a quarterback can get on average 3000 yards. The current highest is 5967.</li>
                        <li>Rushing yards is a measure of how many yards the offensive team gains after rushing the ball which is where the ball is handed off instead of being passed. This statistic is cumulative. Typically over the course of a season a player can get 500 yards. The current highest is 1826.</li>
                        <li>Receiving yards is a measure of how many yards the offensive team gains after a receiver catches the ball from a pass. This statistic is cumulative. Typically over the course of a season a player can get 500 yards. The current highest is 1902.</li>
                        <li>Points allowed per game is a measure of how many points the defensive team gave up against opponents. Typically over the course of the season, the team averages 15.</li>
                        <li>Sacks is a measure of how many times the defensive team tackles the quarterback before they are able to pass the ball(sack them). This statistic is cumulative. Typically over the course of a season, the team averages 38 sacks.</li>
                        
                    </ul>
                </Typography>
            </Box>
            
        </Stack>
    )
}
