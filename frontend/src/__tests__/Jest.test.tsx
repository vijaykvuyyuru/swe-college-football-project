import React from "react";
import { render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import ReactDOM from "react-dom";

import App from "../App";
import Navbar from "../components/Navbar"
import { About } from "../components/About";
import { Coaches } from "../components/Coaches";
import Schools from "../components/Schools";
import Players from "../components/Players";
import CoachesInstance from "../components/CoachesInstance"
import SchoolsInstance from "../components/SchoolsInstance"
import PlayersInstance from "../components/PlayersInstance"

// Much of these tests were made using the following project as a reference:
// https://gitlab.com/JunLum/pride-in-writing/-/blob/main/front-end/src/__tests__/JestTests.test.tsx

describe("Render TuitionBall Components", () => {
  // Test 1
  test("TuitionBall Loads and Sets Document Title", () => {
    <BrowserRouter>
      render(<App />);
      expect(global.window.document.title).toBe('TuitionBall')
    </BrowserRouter>;
  });

  // Test 2
  test("Navbar Loads Correctly", () => {
    <BrowserRouter>
      render(<Navbar />);
      expect(screen.getByText('The Basics')).toBeInTheDocument();
    </BrowserRouter>
  });

  // Test 3
  test("About Page Populates Team Data", () => {
    <BrowserRouter>
      render(<About />);
      expect(screen.getByText('Matt Dombrowski')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 4
  test("About Page Loads Tools Used", () => {
    <BrowserRouter>
      render(<About />);
      expect(screen.getByText('Docker-Compose')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 5
  test("School Model Page Loads Correctly", () => {
    <BrowserRouter>
      render(<Schools />);
      expect(screen.getByText('Schools')).toBeInTheDocument();
    </BrowserRouter>
  });

  // Test 6
  test("Coach Model Page Loads Correctly", () => {
    <BrowserRouter>
      render(<Coaches />);
      expect(screen.getByText('Search for Coaches!')).toBeInTheDocument();
    </BrowserRouter>
  });

  // Test 7
  test("Player Model Page Loads Correctly", () => {
    <BrowserRouter>
      render(<Players />);
      expect(screen.queryByText('Search for players!')).toBeInTheDocument();
    </BrowserRouter>
  });

  // Test 8
  test("School Instance Page Loads Correctly", () => {
    <BrowserRouter>
      render(<SchoolsInstance />);
      expect(screen.queryByText('School Information')).toBeInTheDocument();
    </BrowserRouter>
  });

  // Test 9
  test("Coach Instance Page Loads Correctly", () => {
    <BrowserRouter>
      render(<CoachesInstance />);
      expect(screen.queryByText('Notable Players')).toBeInTheDocument();
    </BrowserRouter>
  });

  // Test 10
  test("Player Instance Page Loads Correctly", () => {
    <BrowserRouter>
      render(<Players />);
      expect(screen.queryByText('Map of Hometown')).toBeInTheDocument();
    </BrowserRouter>
  });

  // Test 11
  test("Search Renders Correctly", () => {
    <BrowserRouter>
      render(<App />);
      expect(screen.queryByText('Search')).toBeInTheDocument();
    </BrowserRouter>
  });

  // Test 12
  test("Filter On Instance Renders Correctly", () => {
    <BrowserRouter>
      render(<Schools />);
      expect(screen.queryByText('Filter')).toBeInTheDocument();
    </BrowserRouter>
  });

  // Test 13
  test("Search on Instance Renders Correctly", () => {
    <BrowserRouter>
      render(<Schools />);
      expect(screen.queryByText('Search')).toBeInTheDocument();
    </BrowserRouter>
  });

  // Test 14
  test("Sort on Instance Renders Correctly", () => {
    <BrowserRouter>
      render(<Schools />);
      expect(screen.queryByText('Sort By')).toBeInTheDocument();
    </BrowserRouter>
  });

  // Test 15
  test("Filter On Instance Renders Correctly", () => {
    <BrowserRouter>
      render(<Players />);
      expect(screen.queryByText('Filter')).toBeInTheDocument();
    </BrowserRouter>
  });

  // Test 16
  test("Search on Instance Renders Correctly", () => {
    <BrowserRouter>
      render(<Players />);
      expect(screen.queryByText('Search')).toBeInTheDocument();
    </BrowserRouter>
  });

  // Test 17
  test("Sort on Instance Renders Correctly", () => {
    <BrowserRouter>
      render(<Players />);
      expect(screen.queryByText('Sort By')).toBeInTheDocument();
    </BrowserRouter>
  });

  // Test 18
  test("Filter On Instance Renders Correctly", () => {
    <BrowserRouter>
      render(<Coaches />);
      expect(screen.queryByText('Filter')).toBeInTheDocument();
    </BrowserRouter>
  });

  // Test 19
  test("Search on Instance Renders Correctly", () => {
    <BrowserRouter>
      render(<Coaches />);
      expect(screen.queryByText('Search')).toBeInTheDocument();
    </BrowserRouter>
  });

  // Test 20
  test("Sort on Instance Renders Correctly", () => {
    <BrowserRouter>
      render(<Coaches />);
      expect(screen.queryByText('Sort By')).toBeInTheDocument();
    </BrowserRouter>
  });
});
