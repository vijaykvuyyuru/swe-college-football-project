import React, {useEffect} from 'react';
import { BrowserRouter as Router, Route, Routes} from "react-router-dom";
import { ThemeProvider } from '@mui/material/styles';
import './App.css';
import Navbar from './components/Navbar';
import { Home } from './components/Home';
import Players from './components/Players';
import { Coaches } from './components/Coaches';
import  Schools  from './components/Schools'
import { About } from './components/About'
import { TheBasics } from './components/TheBasics';
import PlayersInstance from './components/PlayersInstance';
import CoachesInstance from './components/CoachesInstance';
import { theme } from './components/utils/theme';
import SchoolsInstance from './components/SchoolsInstance';
import SearchPage from './components/SearchPage';
import Visualizations from './components/Visualizations';
import VisualizationsProvider from './components/VisualizationsProvider';

function App() {
  useEffect(() => {
    document.title = "TuitionBall"
  }, []);
  return (
    <ThemeProvider theme={theme}>
      <Router>
      <Navbar />
        <Routes>
          <Route path='/players'  element={<Players/>} />
          <Route path='/coaches'  element={<Coaches/>} />
          <Route path='/schools'  element ={<Schools/>} />
          <Route path='/about'    element={<About/>} />
          <Route path='/theBasics'  element={<TheBasics/>} />
          <Route path='/PlayersInstance' element={<PlayersInstance/>} />
          <Route path='/CoachesInstance' element={<CoachesInstance/>} />
          <Route path='/SchoolsInstance' element = {<SchoolsInstance/>} />
          <Route path='/SearchPage' element = {<SearchPage/>} />
          <Route path='/visualizations' element = {<Visualizations/>}/>
          <Route path='/visualizationsProvider' element = {<VisualizationsProvider/>}/>
          <Route path='/'  element={<Home/>} />
        </Routes>
      </Router>
    </ThemeProvider>
  );
}

export default App;
