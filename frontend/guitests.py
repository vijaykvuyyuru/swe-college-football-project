# Unit test code pulled from https://gitlab.com/thomaspeavler2/cs373-project/-/blob/master/front_end/guitests.py
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select

import time


class SeleniumTests(unittest.TestCase):
    def createDriver(self):
        chrome_options = Options()
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--window-size=1420,1080")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.url = "https://dev.tuitionball.me"
        return webdriver.Chrome(options=chrome_options)

    # Routing Tests:
    # Tests routing from buttons on splash page to school page
    def testSplashPageSchoolRouting(self):
        driver = self.createDriver()
        driver.get(self.url)
        time.sleep(2)
        driver.find_element(
            By.XPATH, "/html/body/div/div[2]/div[5]/div[1]/div[3]/button"
        ).click()

        self.assertEqual("https://dev.tuitionball.me/schools", driver.current_url)
        driver.quit()

    # Tests routing from buttons on splash page to coach page
    def testSplashPageCoachRouting(self):
        driver = self.createDriver()
        driver.get(self.url)
        time.sleep(2)
        driver.find_element(
            By.XPATH, "/html/body/div/div[2]/div[5]/div[2]/div[3]/button"
        ).click()

        self.assertEqual("https://dev.tuitionball.me/coaches", driver.current_url)
        driver.quit()

    # Tests routing from buttons on splash page to players page
    def testSplashPageCoachRouting(self):
        driver = self.createDriver()
        driver.get(self.url)
        time.sleep(2)
        driver.find_element(
            By.XPATH, "/html/body/div/div[2]/div[5]/div[3]/div[3]/button"
        ).click()

        self.assertEqual("https://dev.tuitionball.me/players", driver.current_url)
        driver.quit()

    # Tests Routing to Basics page
    def testHomeToBasics(self):
        driver = self.createDriver()
        driver.get(self.url)
        time.sleep(2)
        driver.find_element(By.XPATH, "//a[@href='/theBasics']").click()

        self.assertEqual("https://dev.tuitionball.me/theBasics", driver.current_url)
        driver.quit()

    # Tests Routing to Schools page
    def testHomeToSchools(self):
        driver = self.createDriver()
        driver.get(self.url)
        time.sleep(2)
        driver.find_element(By.XPATH, "//a[@href='/schools']").click()

        self.assertEqual("https://dev.tuitionball.me/schools", driver.current_url)
        driver.quit()

    # Tests Routing to Coaches page
    def testHomeToCoaches(self):
        driver = self.createDriver()
        driver.get(self.url)
        time.sleep(2)
        driver.find_element(By.XPATH, "//a[@href='/coaches']").click()

        self.assertEqual("https://dev.tuitionball.me/coaches", driver.current_url)
        driver.quit()

    # Tests Routing to Players page
    def testHomeToPlayers(self):
        driver = self.createDriver()
        driver.get(self.url)
        time.sleep(2)
        driver.find_element(By.XPATH, "//a[@href='/players']").click()

        self.assertEqual("https://dev.tuitionball.me/players", driver.current_url)
        driver.quit()

    # Tests Routing to Schools page
    def testHomeToAboutUs(self):
        driver = self.createDriver()
        driver.get(self.url)
        time.sleep(2)
        driver.find_element(By.XPATH, "//a[@href='/about']").click()

        self.assertEqual("https://dev.tuitionball.me/about", driver.current_url)
        driver.quit()

    # Tests Routing to default page
    def testAboutUsToSchoolsToHome(self):
        driver = self.createDriver()
        driver.get(self.url + "/about")
        time.sleep(2)
        driver.find_element(By.XPATH, "//a[@href='/schools']").click()

        self.assertEqual("https://dev.tuitionball.me/schools", driver.current_url)

        driver.find_element(By.XPATH, "//a[@href='/']").click()

        self.assertEqual("https://dev.tuitionball.me/", driver.current_url)
        driver.quit()

    # Instance tests:
    # Tests view school instance
    def testViewSchoolInstance(self):
        driver = self.createDriver()
        driver.get(self.url + "/schools")
        time.sleep(5)

        school = driver.find_element(
            By.XPATH, "/html/body/div/div[2]/div[3]/table/tbody/tr[1]/td[1]"
        ).text
        driver.find_element(
            By.XPATH, "/html/body/div/div[2]/div[3]/table/tbody/tr[1]/td[1]"
        ).click()
        time.sleep(5)
        school_instance_name = driver.find_element(
            By.XPATH, "/html/body/div/div[2]/div[1]/h2/b"
        ).text

        self.assertEqual(school, school_instance_name)
        driver.quit()

    # Tests view coach instance
    def testViewCoachInstance(self):
        driver = self.createDriver()
        driver.get(self.url + "/coaches")
        time.sleep(5)

        coach = driver.find_element(
            By.XPATH, "/html/body/div/div[2]/div[2]/div[1]/div/button/div/div"
        ).text
        driver.find_element(
            By.XPATH, "/html/body/div/div[2]/div[2]/div[1]/div/button/div/div"
        ).click()
        time.sleep(5)
        coach_instance_name = driver.find_element(
            By.XPATH, "/html/body/div/div[2]/div[1]/div/div[2]/h1"
        ).text

        self.assertEqual(coach, coach_instance_name[: len(coach)])
        driver.quit()

    # Tests view player instance
    def testViewPlayerInstance(self):
        driver = self.createDriver()
        driver.get(self.url + "/players")
        time.sleep(5)

        player = driver.find_element(
            By.XPATH, "/html/body/div/div[2]/div[2]/div[1]/div/button/div/div"
        ).text
        driver.find_element(
            By.XPATH, "/html/body/div/div[2]/div[2]/div[1]/div/button/div/div"
        ).click()
        time.sleep(5)
        player_instance_name = driver.find_element(
            By.XPATH, "/html/body/div/div[2]/div[1]/div/div[2]/h1"
        ).text

        self.assertEqual(player, player_instance_name[: len(player)])
        driver.quit()

    # Tests filtering of schools
    def testFilteringSchools(self):
        driver = self.createDriver()
        driver.get(self.url + "/schools")
        time.sleep(5)
        driver.find_element(By.XPATH, "/html/body/div/div[2]/div[2]/div[2]/div[3]/div/div/div/div").click()
        driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/ul/li[2]").click()
        time.sleep(2)
        number_private_schools = driver.find_element(By.XPATH, "/html/body/div/div[2]/div[5]/h5/b").text
        self.assertEqual(number_private_schools, 'Total Schools: 17')

    def testSortingSchools(self):
        driver = self.createDriver()
        driver.get(self.url + "/schools")
        time.sleep(5)
        driver.find_element(By.XPATH, "/html/body/div/div[2]/div[2]/div[2]/div[2]/div/div/div/div").click()
        driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/ul/li[2]").click()
        time.sleep(2)
        self.assertEqual('West Virginia University',
                         driver.find_element(By.XPATH, "/html/body/div/div[2]/div[3]/table/tbody/tr[1]/td[1]").text)

    def testSearchingSchools(self):
        driver = self.createDriver()
        driver.get(self.url + "/schools")
        time.sleep(5)
        input = driver.find_element(By.XPATH, "/html/body/div/div[2]/div[2]/div[1]/div/div/div/input")
        input.click()
        input.send_keys('jason')
        input.send_keys(Keys.ENTER)
        time.sleep(4)
        self.assertEqual('University of Akron Main Campus',
                         driver.find_element(By.XPATH, "/html/body/div/div[2]/div[3]/table/tbody/tr[1]/td[1]").text)


    # Tests filtering of players
    def testFilteringPlayers(self):
        driver = self.createDriver()
        driver.get(self.url + "/players")
        time.sleep(5)
        driver.find_element(By.XPATH, "/html/body/div/div[2]/div[1]/div[2]/div[3]/div/div/div/div").click()
        driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/ul/li[1]").click()
        time.sleep(3)
        number_quarterbacks = driver.find_element(By.XPATH, "/html/body/div/div[2]/div[4]").text
        self.assertEqual(number_quarterbacks, 'Total Number of Instances: 302')

    def testSortingPlayers(self):
        driver = self.createDriver()
        driver.get(self.url + "/players")
        time.sleep(5)
        driver.find_element(By.XPATH, "/html/body/div/div[2]/div[1]/div[2]/div[6]/div/div/div/div").click()
        driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/ul/li[2]").click()
        time.sleep(2)
        self.assertEqual('Zyell Griffin',
                         driver.find_element(By.XPATH, "/html/body/div/div[2]/div[2]/div[1]/div/button/div/div").text)

    def testSearchingPlayers(self):
        driver = self.createDriver()
        driver.get(self.url + "/players")
        time.sleep(5)
        input = driver.find_element(By.XPATH, "/html/body/div/div[2]/div[1]/div[1]/div/div/div/input")
        input.click()
        input.send_keys('boobie')
        input.send_keys(Keys.ENTER)
        time.sleep(4)
        self.assertEqual('Boobie Curry',
                         driver.find_element(By.XPATH, "/html/body/div/div[2]/div[2]/div/div/button/div/div/span").text)

    def testFilteringCoaches(self):
        driver = self.createDriver()
        driver.get(self.url + "/coaches")
        time.sleep(5)
        input = driver.find_element(By.XPATH, "/html/body/div/div[2]/div[1]/div[2]/div[3]/div/div/div/div/input")
        input.click()
        input.send_keys('1998')
        input.send_keys(Keys.ENTER)
        time.sleep(4)
        number_coaches = driver.find_element(By.XPATH, "/html/body/div/div[2]/div[4]").text
        self.assertEqual(number_coaches, 'Total Number of Instances: 132')

    def testSortingCoaches(self):
        driver = self.createDriver()
        driver.get(self.url + "/coaches")
        time.sleep(5)
        driver.find_element(By.XPATH, "/html/body/div/div[2]/div[1]/div[2]/div[6]/div/div/div/div").click()
        driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/ul/li[2]").click()
        time.sleep(2)
        self.assertEqual('Willie Fritz',
                         driver.find_element(By.XPATH, "/html/body/div/div[2]/div[2]/div[1]/div/button/div/div").text)

    def testSearchingCoaches(self):
        driver = self.createDriver()
        driver.get(self.url + "/coaches")
        time.sleep(5)
        input = driver.find_element(By.XPATH, "/html/body/div/div[2]/div[1]/div[1]/div/div/div/input")
        input.click()
        input.send_keys('steve')
        input.send_keys(Keys.ENTER)
        time.sleep(4)
        self.assertEqual('Steve Addazio',
                         driver.find_element(By.XPATH, "/html/body/div/div[2]/div[2]/div[1]/div/button/div/div/span").text)

if __name__ == '__main__':
    unittest.main()
