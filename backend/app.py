from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_
from enum import Enum
from models import *
import datetime

app = Flask(__name__)
app.config["JSON_SORT_KEYS"] = False
CORS(app)
db = init_db(app)

# Errors
id_not_found = "ID not found"

# Constants
DEFAULT_NUM_PER_PAGE = 20

# Enums
class ModelType(Enum):
    School = 1
    Player = 2
    Coach = 3


school_schema = SchoolSchema()
player_schema = PlayerSchema()
coach_schema = CoachSchema()
coach_history_schema = CoachHistorySchema()

# Default route: Rickroll ;)
@app.route("/")
def hello_world():
    return '<img src="https://media4.giphy.com/media/g7GKcSzwQfugw/giphy.gif" alt="rickroll" />'

# Global search of the database
@app.route("/search/<search_params>", methods=["GET"])
def search_all(search_params):
    school_query = search_schools(School.query, search_params)  # Search all schools with search_params
    player_query = search_players(Player.query, search_params)  # Search all players with search_params
    coach_query = search_coaches(Coach.query, search_params)    # Search all coaches with search_params

    # Combine all results into the response JSON
    response = {}
    response['players'] = [convert_player_to_json(player, True) for player in player_query]
    response['schools'] = [convert_school_to_json(school, True) for school in school_query]
    response['coaches'] = [convert_coach_to_json(coach, True) for coach in coach_query]

    return jsonify(response)

# Schools route
@app.route("/schools", methods=["GET"])
def get_schools():
    args = request.args.to_dict(flat=False)

    sparse = False
    if "sparse" in args:
        sparse = args["sparse"][0] == "true"

    school_query = School.query # initialize school query

    # First check if it's getting a school by id
    if "id" in args:
        school = school_query.filter_by(id=args["id"][0]).first()   # get school with correct ID
        
        # Return error if school id not found
        if school is None:
            response = {"error": id_not_found}
            return response, 404

        response = convert_school_to_json(school, sparse)
        return jsonify(response)

    # Apply search function to query if search parameter specified
    school_query = (
        search_schools(school_query, args["search"][0])
        if "search" in args
        else school_query
    )

    # Filter the query using the URL args
    school_query = filter_schools(school_query, args)

    # Sort the query if the sort parameter is specified
    school_query = (
        sort_instances(ModelType.School, school_query, args["sort"][0])
        if "sort" in args
        else school_query
    )

    # Paginate the response
    page = int(args["page"][0]) if "page" in args else 1
    per_page = int(args["per-page"][0]) if "per-page" in args else DEFAULT_NUM_PER_PAGE 
    schools = school_query.paginate(page=page, per_page=per_page)

    # Convert each of the schools in the query to json
    data = [convert_school_to_json(school, sparse) for school in schools.items]

    num_records_total = school_query.count()    # Get total number of records

    # Calculate total num of pages
    num_pages = -(
        num_records_total // -per_page
    )  # Upside-down flor division (ceil division)

    prev_page = page - 1
    next_page = page + 1

    # Create JSON response
    response = {}
    response["data"] = data
    response["records_per_page"] = per_page
    response["total_num_records"] = num_records_total
    response["num_pages"] = num_pages

    if prev_page in range(1, num_pages + 1):
        response["prev_page"] = prev_page
    response["curr_page"] = page
    if next_page in range(1, num_pages + 1):
        response["next_page"] = next_page

    return jsonify(response)

# Players route
@app.route("/players", methods=["GET"])
def get_players():
    args = request.args.to_dict(flat=False)

    sparse = False
    if "sparse" in args:
        sparse = args["sparse"][0] == "true"

    player_query = Player.query # initialize player query

    # First check if it's getting a player by id
    if "id" in args:
        player = player_query.filter_by(id=args["id"][0]).first()   # get player with correct ID
        
        # Return error if player id not found
        if player is None:
            response = {"error": id_not_found}
            return response, 404

        response = convert_player_to_json(player, sparse)
        return jsonify(response)

    # Apply search function to query if search parameter specified
    player_query = (
        search_players(player_query, args["search"][0])
        if "search" in args
        else player_query
    )

    # Filter the query using the URL args
    player_query = filter_players(player_query, args)

    # Sort the query if the sort parameter is specified
    player_query = (
        sort_instances(ModelType.Player, player_query, args["sort"][0])
        if "sort" in args
        else player_query
    )

    # Paginate the response
    page = int(args["page"][0]) if "page" in args else 1
    per_page = int(args["per-page"][0]) if "per-page" in args else DEFAULT_NUM_PER_PAGE
    players = player_query.paginate(page=page, per_page=per_page)

    # Convert each of the players in the query to json
    data = [convert_player_to_json(player, sparse) for player in players.items]

    num_records_total = player_query.count()    # Get total number of records

    # Calculate total num of pages
    num_pages = -(
        num_records_total // -per_page
    )  # Upside-down flor division (ceil division)
    prev_page = page - 1
    next_page = page + 1

    # Create JSON response
    response = {}
    response["data"] = data
    response["records_per_page"] = per_page
    response["total_num_records"] = num_records_total
    response["num_pages"] = num_pages

    if prev_page in range(1, num_pages + 1):
        response["prev_page"] = prev_page
    response["curr_page"] = page
    if next_page in range(1, num_pages + 1):
        response["next_page"] = next_page

    return jsonify(response)

# Coaches route
@app.route("/coaches", methods=["GET"])
def get_coaches():
    args = request.args.to_dict(flat=False)

    sparse = False
    if "sparse" in args:
        sparse = args["sparse"][0] == "true"

    coach_query = Coach.query # initialize coach query

    # First check if it's getting a coach by id
    if "id" in args:
        coach = coach_query.filter_by(id=args["id"][0]).first()   # get coach with correct ID
        
        # Return error if coach id not found
        if coach is None:
            response = {"error": id_not_found}
            return response, 404

        response = convert_coach_to_json(coach, sparse)
        return jsonify(response)

    # Apply search function to query if search parameter specified
    coach_query = (
        search_coaches(coach_query, args["search"][0])
        if "search" in args
        else coach_query
    )

    # Filter the query using the URL args
    coach_query = filter_coaches(coach_query, args)
    
    # Sort the query if the sort parameter is specified
    coach_query = (
        sort_instances(ModelType.Coach, coach_query, args["sort"][0])
        if "sort" in args
        else coach_query
    )

    # Paginate the response
    page = int(args["page"][0]) if "page" in args else 1
    per_page = int(args["per-page"][0]) if "per-page" in args else DEFAULT_NUM_PER_PAGE
    coaches = coach_query.paginate(page=page, per_page=per_page)

    # Convert each of the coaches in the query to json
    data = [convert_coach_to_json(coach, sparse) for coach in coaches.items]

    num_records_total = coach_query.count()    # Get total number of records

    # Calculate total num of pages
    num_pages = -(
        num_records_total // -per_page
    )  # Upside-down flor division (ceil division)
    prev_page = page - 1
    next_page = page + 1

    # Create JSON response
    response = {}
    response["data"] = data
    response["records_per_page"] = per_page
    response["total_num_records"] = num_records_total
    response["num_pages"] = num_pages

    if prev_page in range(1, num_pages + 1):
        response["prev_page"] = prev_page
    response["curr_page"] = page
    if next_page in range(1, num_pages + 1):
        response["next_page"] = next_page

    return jsonify(response)


# Attributes by which schools can be sorted
school_sort_attr = {
    "size": School.size,
    "tuition": School.tuition,
    "school": School.school,
}

# Attributes by which players can be sorted
player_sort_attr = {"first_name": Player.first_name, "last_name": Player.last_name}

# Attributes by which coaches can be sorted
coach_sort_attr = {
    "first_name": Coach.first_name,
    "last_name": Coach.last_name,
    "salary": Coach.salary,
    "hire_date": Coach.hire_date,
}

# Map of ModelType (School, Player, Coach) to their sortable attributes
sort_attribute_map = {
    ModelType.School: school_sort_attr,
    ModelType.Player: player_sort_attr,
    ModelType.Coach: coach_sort_attr,
}


# Sorts the query based on the model type and the sort_param
def sort_instances(modelType, query, sort_param):
    attributes = sort_attribute_map[modelType]  # Get sortable attributes for this model

    # Determine if we should be sorting in descending or ascending order
    split_params = sort_param.split("-")
    sort = split_params[0]
    descending = False
    if len(split_params) == 2 and split_params[1] == "desc":
        descending = True

    # If the sort param is not found in the sortable attributes, return the query
    if sort not in attributes:
        return query

    attribute = attributes[sort]

    # Special Case for Coach Salaries -> Filter out if salary == 0
    if modelType == ModelType.Coach and sort == "salary":
        query = query.filter(Coach.salary != 0)

    # Sort the query and return
    if descending:
        return query.order_by(attribute.desc())
    else:
        return query.order_by(attribute)


# Filters schools
def filter_schools(query, args):
    if "type" in args: # filter by school type
        query = query.filter(School.type == args["type"][0])
    if "conference" in args: # filter by school conference
        query = query.filter(School.conference == args["conference"][0])

    return query


# Filters players
def filter_players(query, args):
    if "team" in args: # filter by player team
        # Get the school the 'team' param refers to
        school = School.query.filter(School.school == args["team"][0]).first()

        # If school isn't found, return an empty query set
        if school is None:
            return query.filter(False)

        # Filter player query by id of found school
        query = query.filter(Player.school_id == school.id)

    if "position" in args: # filter by player position
        query = query.filter(Player.position == args["position"][0])

    if "year" in args: # filter by player year
        year_dict = {"freshman": 1, "sophomore": 2, "junior": 3, "senior": 4}
        year_name = args["year"][0]
        if year_name in year_dict:
            query = query.filter(Player.year == year_dict[year_name])

    return query


# Filters coaches
def filter_coaches(query, args):
    if "team" in args: # filter coach by team
        # Get the school the 'team' param refers to
        school = School.query.filter(School.school == args["team"][0]).first()

        # If school isn't found, return an empty query set
        if school is None:
            return query.filter(False)

        # Filter coach query by id of found school
        query = query.filter(Coach.school_id == school.id)

    if "hired_before" in args: # filter coach by the date they were hired (before)
        query = query.filter(
            Coach.hire_date
            <= datetime.datetime.strptime(args["hired_before"][0], "%Y-%m-%dT%H:%M:%S")
        )

    if "hired_after" in args: # filter coach by the date they were hired (after)
        query = query.filter(
            Coach.hire_date
            >= datetime.datetime.strptime(args["hired_after"][0], "%Y-%m-%dT%H:%M:%S")
        )

    return query


# School search
def search_schools(query, search_params):
    terms = search_params.split() # split search params into individual search terms

    '''
    Build a list of filters to apply to the query. Each search term is used to create filters
    for the searchable attributes within the school model. 
    '''
    search_hits = []
    for term in terms:
        search_hits.append(School.school.ilike("%{}%".format(term.lower())))
        search_hits.append(School.school_nickname.ilike("%{}%".format(term.lower())))
        search_hits.append(School.mascot.ilike("%{}%".format(term.lower())))
        search_hits.append(School.color.ilike("%{}%".format(term.lower())))
        search_hits.append(School.conference.ilike("%{}%".format(term.lower())))
        search_hits.append(School.division.ilike("%{}%".format(term.lower())))
        search_hits.append(School.type.ilike("%{}%".format(term.lower())))
        search_hits.append(School.location.ilike("%{}%".format(term.lower())))
        search_hits.append(School.nfl_players.ilike("%{}%".format(term.lower())))
    
    # filter the query by "or-ing" all of the filters in the list together
    return query.filter(or_(*(search_hits)))

# Player search
def search_players(query, search_params):
    terms = search_params.split() # split search params into individual search terms

    '''
    Build a list of filters to apply to the query. Each search term is used to create filters
    for the searchable attributes within the player model. 
    '''
    search_hits = []
    for term in terms:
        years={'freshman': 1, 'sophomore': 2, 'junior': 3, 'senior': 4}
        search_hits.append(Player.first_name.ilike("%{}%".format(term.lower())))
        search_hits.append(Player.last_name.ilike("%{}%".format(term.lower())))
        search_hits.append(Player.position.ilike("%{}%".format(term.lower())))
        search_hits.append(Player.home_city.ilike("%{}%".format(term.lower())))
        search_hits.append(Player.home_state.ilike("%{}%".format(term.lower())))
        
        if term.lower() in years:
            search_hits.append(Player.year == years[term.lower()])
        
        '''
        Special case: get the name of the school a search term may refer to.
        If a school is found, compare the id of the school to the school_id in the
        player model
        '''
        school = School.query.filter(School.school.ilike("%{}%".format(term.lower()))).first()
        if school is not None:
            search_hits.append(Player.school_id == school.id)

    # filter the query by "or-ing" all of the filters in the list together
    return query.filter(or_(*(search_hits)))

# Coach search
def search_coaches(query, search_params):
    terms = search_params.split() # split search params into individual search terms

    '''
    Build a list of filters to apply to the query. Each search term is used to create filters
    for the searchable attributes within the coach model. 
    '''
    search_hits = []
    for term in terms:
        search_hits.append(Coach.first_name.ilike("%{}%".format(term.lower())))
        search_hits.append(Coach.last_name.ilike("%{}%".format(term.lower())))
        
        '''
        Special case: get the name of the school a search term may refer to.
        If a school is found, compare the id of the school to the school_id in the
        coach model
        '''
        school = School.query.filter(School.school.ilike("%{}%".format(term.lower()))).first()
        if school is not None:
            search_hits.append(Coach.school_id == school.id)    

    # filter the query by "or-ing" all of the filters in the list together
    return query.filter(or_(*(search_hits)))

# Function to convert a school instance to a JSON 
def convert_school_to_json(school, sparse):
    school_json = {}

    # If sparse is enabled, only construct a JSON with minimal attributes included
    if sparse:
        school_json["id"] = school.id
        school_json["school"] = school.school
        school_json["size"] = school.size
        school_json["tuition"] = school.tuition
        school_json["type"] = school.type
        school_json["conference"] = school.conference
        school_json["logo"] = school.logo
    else: # If sparse is not enabled, construct JSON with all atrributes
        school_json = school_schema.dump(school)

    return school_json


# Function to convert a player instance to a JSON 
def convert_player_to_json(player, sparse):
    player_json = {}

    # If sparse is enabled, only construct a JSON with minimal attributes included
    if sparse:
        player_json["id"] = player.id
        player_json["first_name"] = player.first_name
        player_json["last_name"] = player.last_name

        # Get the name of the school by filtering a school query by the school_id attribute
        player_json["school"] = (
            School.query.filter(School.id == player.school_id).first().school
        )

        player_json["position"] = player.position
        player_json["year"] = ClassEnum(player.year).name # convert player.year to string
    else: # If sparse is not enabled, construct JSON with all atrributes
        player_json = player_schema.dump(player)

        # Get the name of the school by filtering a school query by the school_id attribute
        player_json["school"] = (
            School.query.filter(School.id == player_json["school_id"]).first().school
        )

        player_json["year"] = ClassEnum(player_json["year"]).name # convert player.year to string

    return player_json


# Function to convert a coach instance to a JSON 
def convert_coach_to_json(coach, sparse):
    coach_json = {}

    '''
    Note: the sparse parameter is most important for the coach model.
    Since coaches have coach_history objects related to them, the speed
    of the API call significantly decreases when all of the history objects
    are included in the response. The sparse parameter suppresses this behavior,
    resulting in a massive speedup. This is especially useful on our model pages
    since we do not need to include all of the data we have on a coach. The sparse
    parameter should not be used on instance pages.
    '''

    # If sparse is enabled, only construct a JSON with minimal attributes included
    if sparse:
        coach_json["id"] = coach.id
        coach_json["first_name"] = coach.first_name
        coach_json["last_name"] = coach.last_name
        coach_json["salary"] = coach.salary if coach.salary != 0 else "Not found"

        # Get the name of the school by filtering a school query by the school_id attribute
        coach_json["Current_school"] = (
            School.query.filter(School.id == coach.school_id).first().school
        )

        coach_json["hire_date"] = coach.hire_date
        coach_json["image"] = coach.image
        coach_json["overall_wins"] = coach.overall_wins
        coach_json["overall_losses"] = coach.overall_losses
        coach_json["current_wins"] = coach.current_wins
        coach_json["current_losses"] = coach.current_losses
        coach_json["school_id"] = coach.school_id

    else: # If sparse is not enabled, construct JSON with all atrributes
        coach_json = coach_schema.dump(coach)

        # Get the name of the school by filtering a school query by the school_id attribute
        coach_json["Current_school"] = (
            School.query.filter(School.id == coach_json["school_id"]).first().school
        )

        # Build the list of previous seasons (history) for the coach
        history_json_arr = []
        coach_history = CoachHistory.query.filter(
            CoachHistory.coach_id == coach_json["id"]
        )
        for prev_season in coach_history.all():
            prev_season_json = coach_history_schema.dump(prev_season)
            prev_season_json["school"] = (
                School.query.filter(School.id == prev_season_json["school_id"])
                .first()
                .school
            )
            history_json_arr.append(prev_season_json)
        coach_json["history"] = history_json_arr

        coach_json["salary"] = (
            coach_json["salary"] if coach_json["salary"] != 0 else "Not found"
        )

    return coach_json


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
