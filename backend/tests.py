"""
Python unit tests for the API
"""

import unittest
import app
from sort_verify import *

app.app.testing = True

test_client = app.app.test_client()

invalid_response = {"error": "ID not found"}


class BackendTest(unittest.TestCase):
    def test_get_all_coaches(self):
        response = test_client.get("/coaches?per-page=3&page=2")
        self.assertEqual(response.status_code, 200)

        response_json = response.json

        self.assertIsInstance(response_json, dict)

        data = response_json["data"]
        self.assertIsInstance(data, list)
        self.assertEqual(len(data), 3)

        target_response = {
            "id": 4,
            "first_name": "Blake",
            "last_name": "Anderson",
            "hire_date": "2020-12-12T00:00:00",
            "current_wins": 11,
            "current_losses": 3,
            "overall_wins": 62,
            "overall_losses": 40,
            "salary": "Not found",
            "image": "//upload.wikimedia.org/wikipedia/commons/thumb/9/90/SunBeltMD-2015-0720-BlakeAnderson.png/220px-SunBeltMD-2015-0720-BlakeAnderson.png",
            "school_id": 117,
            "school_logo": "http://a.espncdn.com/i/teamlogos/ncaa/500/328.png",
            "Current_school": "Utah State University",
            "history": [
                {
                    "id": 16,
                    "season": 2014,
                    "wins": 7,
                    "losses": 6,
                    "ties": 0,
                    "preseason_rank": None,
                    "postseason_rank": None,
                    "coach_id": 4,
                    "school_id": 8,
                    "school": "Arkansas State University",
                },
                {
                    "id": 17,
                    "season": 2015,
                    "wins": 9,
                    "losses": 4,
                    "ties": 0,
                    "preseason_rank": None,
                    "postseason_rank": None,
                    "coach_id": 4,
                    "school_id": 8,
                    "school": "Arkansas State University",
                },
                {
                    "id": 18,
                    "season": 2016,
                    "wins": 8,
                    "losses": 5,
                    "ties": 0,
                    "preseason_rank": None,
                    "postseason_rank": None,
                    "coach_id": 4,
                    "school_id": 8,
                    "school": "Arkansas State University",
                },
                {
                    "id": 19,
                    "season": 2017,
                    "wins": 7,
                    "losses": 5,
                    "ties": 0,
                    "preseason_rank": None,
                    "postseason_rank": None,
                    "coach_id": 4,
                    "school_id": 8,
                    "school": "Arkansas State University",
                },
                {
                    "id": 20,
                    "season": 2018,
                    "wins": 8,
                    "losses": 5,
                    "ties": 0,
                    "preseason_rank": None,
                    "postseason_rank": None,
                    "coach_id": 4,
                    "school_id": 8,
                    "school": "Arkansas State University",
                },
                {
                    "id": 21,
                    "season": 2019,
                    "wins": 8,
                    "losses": 5,
                    "ties": 0,
                    "preseason_rank": None,
                    "postseason_rank": None,
                    "coach_id": 4,
                    "school_id": 8,
                    "school": "Arkansas State University",
                },
                {
                    "id": 22,
                    "season": 2020,
                    "wins": 4,
                    "losses": 7,
                    "ties": 0,
                    "preseason_rank": None,
                    "postseason_rank": None,
                    "coach_id": 4,
                    "school_id": 8,
                    "school": "Arkansas State University",
                },
            ],
        }
        self.assertDictEqual(data[0], target_response)

        self.assertEqual(response_json["records_per_page"], 3)
        self.assertEqual(response_json["total_num_records"], 132)
        self.assertEqual(response_json["num_pages"], 44)
        self.assertEqual(response_json["prev_page"], 1)
        self.assertEqual(response_json["curr_page"], 2)
        self.assertEqual(response_json["next_page"], 3)

    def test_get_single_coach(self):
        single_coach = test_client.get("/coaches?id=5")
        self.assertEqual(single_coach.status_code, 200)
        self.assertIsInstance(single_coach.json, dict)
        target_response = {
            "id": 5,
            "first_name": "Dave",
            "last_name": "Aranda",
            "hire_date": "2020-01-22T00:00:00",
            "current_wins": 14,
            "current_losses": 9,
            "overall_wins": 14,
            "overall_losses": 9,
            "salary": "Not found",
            "image": "//upload.wikimedia.org/wikipedia/commons/d/d7/Aranda_2021.jpg",
            "school_id": 12,
            "school_logo": "http://a.espncdn.com/i/teamlogos/ncaa/500/239.png",
            "Current_school": "Baylor University",
            "history": [
                {
                    "id": 23,
                    "season": 2020,
                    "wins": 2,
                    "losses": 7,
                    "ties": 0,
                    "preseason_rank": None,
                    "postseason_rank": None,
                    "coach_id": 5,
                    "school_id": 12,
                    "school": "Baylor University",
                }
            ],
        }
        self.assertDictEqual(single_coach.json, target_response)

    def test_get_invalid_coach(self):
        missing_coach = test_client.get("/coaches?id=-1")
        self.assertEqual(missing_coach.status_code, 404)
        self.assertIsInstance(missing_coach.json, dict)
        self.assertDictEqual(missing_coach.json, invalid_response)

    def test_get_all_schools(self):
        response = test_client.get("/schools?per-page=3&page=2")
        self.assertEqual(response.status_code, 200)

        response_json = response.json

        self.assertIsInstance(response_json, dict)

        data = response_json["data"]
        self.assertIsInstance(data, list)
        self.assertEqual(len(data), 3)

        target_response = {
            "id": 4,
            "school": "Appalachian State University",
            "school_nickname": "Appalachian State",
            "mascot": "Mountaineers",
            "color": "#000000",
            "conference": "Sun Belt",
            "division": None,
            "size": 17401,
            "tuition": 13920,
            "type": "public",
            "location": "Kidd Brewer Stadium",
            "wins": 10,
            "losses": 4,
            "ties": None,
            "recruiting_class_rank": 69,
            "nfl_players": "Brian Quick Wide Receiver 33 Los Angeles,Dino Hackett Linebacker 35 Kansas City,Dexter Jackson Wide Receiver 58 Tampa Bay,Dexter Coakley Linebacker 65 Dallas,Harold Alexander Punter 67 Atlanta,Armanti Edwards Wide Receiver 89 Carolina,Darrynton Evans Running Back 93 Tennessee,Akeem Davis-Gaither Outside Linebacker 107 Cincinnati,Gary Dandridge Defensive Back 122 Seattle,Derrick Graham Offensive Tackle 124 Kansas City,Ronald Blair Defensive Tackle 142 San Francisco,Mark LeGree Safety 156 Seattle,Daniel Kilgore Offensive Guard 163 San Francisco,Sam Martin Punter 165 Detroit,Corey Lynch Safety 177 Cincinnati,Shemar Jean-Charles Cornerback 178 Green Bay,Mike Frier Defensive End 178 Seattle,D.J. Smith Outside Linebacker 186 Green Bay,Keith Collins Defensive Back 193 Los Angeles,Demetrius McCray Cornerback 210 Jacksonville,Colby Gossett Offensive Guard 213 Minnesota,Corey Hall Defensive Back 215 Atlanta,",
            "passing_yards": 3487,
            "logo": "http://a.espncdn.com/i/teamlogos/ncaa/500/2026.png",
        }
        self.assertDictEqual(data[0], target_response)

        self.assertEqual(response_json["records_per_page"], 3)
        self.assertEqual(response_json["total_num_records"], 130)
        self.assertEqual(response_json["num_pages"], 44)
        self.assertEqual(response_json["prev_page"], 1)
        self.assertEqual(response_json["curr_page"], 2)
        self.assertEqual(response_json["next_page"], 3)

    def test_get_single_school(self):
        school_response = test_client.get("/schools?id=2")
        self.assertEqual(school_response.status_code, 200)
        self.assertIsInstance(school_response.json, dict)
        target_response = {
            "id": 2,
            "school": "University of Akron Main Campus",
            "school_nickname": "Akron",
            "mascot": "Zips",
            "color": "#00285e",
            "conference": "Mid-American",
            "division": "East",
            "size": 12744,
            "tuition": 17792,
            "type": "public",
            "location": "Summa Field at InfoCision Stadium",
            "wins": 2,
            "losses": 10,
            "ties": None,
            "recruiting_class_rank": 119,
            "nfl_players": "Charlie Frye Quarterback 67 Cleveland,Jason Taylor Defensive End 73 Miami,Dwight Smith Defensive Back 84 Tampa Bay,Andy Alleman Offensive Guard 88 New Orleans,Reggie Corner Cornerback 114 Buffalo,Domenik Hixon Wide Receiver 130 Denver,Jake Schifino Wide Receiver 151 Tennessee,Jatavis Brown Outside Linebacker 175 Los Angeles,Chris Kelley Tight End 178 Pittsburgh,Ulysees Gilbert III Inside Linebacker 207 Pittsburgh,",
            "passing_yards": 2610,
            "logo": "http://a.espncdn.com/i/teamlogos/ncaa/500/2006.png",
        }
        self.assertDictEqual(school_response.json, target_response)

    def test_get_invalid_school(self):
        school_response = test_client.get("/schools?id=1000023")
        self.assertEqual(school_response.status_code, 404)
        self.assertIsInstance(school_response.json, dict)
        self.assertDictEqual(school_response.json, invalid_response)

    def test_get_all_players(self):
        response = test_client.get("/players?per-page=3&page=2")
        self.assertEqual(response.status_code, 200)

        response_json = response.json

        self.assertIsInstance(response_json, dict)

        data = response_json["data"]
        self.assertIsInstance(data, list)
        self.assertEqual(len(data), 3)

        target_response = {
            "id": 4,
            "first_name": "Michael",
            "last_name": "Wiley",
            "year": "sophomore",
            "position": "RB",
            "height": 71,
            "weight": 202,
            "jersey": 6,
            "home_city": "Houston",
            "home_state": "TX",
            "first_down": 0.1518,
            "second_down": 0.158,
            "third_down": 0.09,
            "picture": "http://arizonawildcats.com/images/2019/8/13/MichaelWiley_alt.jpg",
            "school_id": 5,
            "school": "University of Arizona",
            "coach_first_name": "Jedd",
            "coach_id": 48,
            "coach_last_name": "Fisch",
            "school_logo": "http://a.espncdn.com/i/teamlogos/ncaa/500/12.png",
        }
        self.assertDictEqual(data[0], target_response)

        self.assertEqual(response_json["records_per_page"], 3)
        self.assertEqual(response_json["total_num_records"], 1901)
        self.assertEqual(response_json["num_pages"], 634)
        self.assertEqual(response_json["prev_page"], 1)
        self.assertEqual(response_json["curr_page"], 2)
        self.assertEqual(response_json["next_page"], 3)

    def test_get_single_player(self):
        player_response = test_client.get("/players?id=2")
        self.assertEqual(player_response.status_code, 200)
        self.assertIsInstance(player_response.json, dict)
        target_response = {
            "id": 2,
            "first_name": "Jalen",
            "last_name": "Johnson",
            "year": "sophomore",
            "position": "WR",
            "height": 74,
            "weight": 224,
            "jersey": 3,
            "home_city": "Corona",
            "home_state": "CA",
            "first_down": 0.0027,
            "second_down": 0.014,
            "third_down": 0.0,
            "picture": "http://arizonawildcats.com/images/2019/8/13/JalenJohnson_alt.JPG",
            "school_id": 5,
            "school": "University of Arizona",
            "coach_first_name": "Jedd",
            "coach_id": 48,
            "coach_last_name": "Fisch",
            "school_logo": "http://a.espncdn.com/i/teamlogos/ncaa/500/12.png",
        }
        self.assertDictEqual(player_response.json, target_response)

    def test_get_invalid_player(self):
        player_response = test_client.get("/players?id=1000023")
        self.assertEqual(player_response.status_code, 404)
        self.assertIsInstance(player_response.json, dict)
        self.assertDictEqual(player_response.json, invalid_response)

    def test_coach_sorting(self):
        coaches_response = test_client.get("/coaches?sort=first_name")
        self.assertEqual(coaches_response.status_code, 200)
        results = coaches_response.json["data"]
        L = [obj["first_name"] for obj in results]
        self.assertTrue(is_sorted_asc(L))

    def test_coach_sorting_desc(self):
        coaches_response = test_client.get("/coaches?sort=first_name-desc")
        self.assertEqual(coaches_response.status_code, 200)
        results = coaches_response.json["data"]
        L = [obj["first_name"] for obj in results]
        self.assertTrue(is_sorted_desc(L))

    def test_player_sorting(self):
        players_response = test_client.get("/players?sort=first_name")
        self.assertEqual(players_response.status_code, 200)
        results = players_response.json["data"]
        L = [obj["first_name"] for obj in results]
        self.assertTrue(is_sorted_asc_string(L))

    def test_player_sorting_desc(self):
        players_response = test_client.get("/players?sort=first_name-desc")
        self.assertEqual(players_response.status_code, 200)
        results = players_response.json["data"]
        L = [obj["first_name"] for obj in results]
        self.assertTrue(is_sorted_desc_string(L))

    def test_school_sorting(self):
        schools_response = test_client.get("/schools?sort=size")
        self.assertEqual(schools_response.status_code, 200)
        results = schools_response.json["data"]
        L = [obj["size"] for obj in results]
        self.assertTrue(is_sorted_asc(L))

    def test_school_sorting_desc(self):
        schools_response = test_client.get("/schools?sort=size-desc")
        self.assertEqual(schools_response.status_code, 200)
        results = schools_response.json["data"]
        L = [obj["size"] for obj in results]
        self.assertTrue(is_sorted_desc(L))




if __name__ == "__main__":
    unittest.main()
