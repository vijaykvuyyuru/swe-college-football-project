import json
import requests
from dotenv import load_dotenv
import os
from math import ceil
from os.path import exists
from sqlalchemy import null

filled_schools = []
invalid_schools = ["center", "beauty", "cosmetology", "cosmetology-", "city", "community", "polytechnic", "medicine"]


def getAPIString(page_num=0):
    return "https://api.data.gov/ed/collegescorecard/v1/schools?api_key=" + os.getenv("COLLEGE_DATA_KEY") + "&per_page=100&fields=school.name,latest.student.size,school.ownership,latest.cost.avg_net_price.overall,latest.school.city,latest.school.state" + f"&page={page_num}"


# Query API for data that is not the same endpoint such as student size and formal school name
def querySchoolSupplementaryData():
    if exists("instances/tuition_data.json"):
        with open('instances/tuition_data.json') as data_file:
            print('using cached results')
            return json.load(data_file)
    api_string = getAPIString()
    response = requests.get(api_string)
    jsonified_data = response.json()
    total_records = jsonified_data["metadata"]["total"]
    merged_data = jsonified_data["results"]
    per_page = 100
    # page indexing is 0 based so subtract 1 to get last page
    num_pages = ceil(total_records/per_page) - 1
    cur_page = 1
    
    while cur_page <= num_pages:
        print(f"Querying page {cur_page} of {num_pages}")
        response = requests.get(getAPIString(cur_page))
        jsonified_data = response.json()
        for school in jsonified_data["results"]:
            if not invalidSchoolName(school["school.name"]) and school["latest.student.size"] is not None and \
                    school["latest.student.size"] > 500:
                merged_data.append(school)
        cur_page += 1
    print("done querying data")
    with open("instances/tuition_dataT.json", "w") as f:
        json.dump(merged_data, f)
    return merged_data


def invalidSchoolName(name):
    lower_name = name.lower()
    for bad_name in invalid_schools:
        if lower_name in bad_name:
            return True
    return False


def getMatchingTuitionData(find_school, tuition_data):
    for school in tuition_data:
        if school["latest.school.city"].lower() == find_school["location"]["city"].lower() and school["latest.school.state"].lower() == find_school["location"]["state"].lower() and not invalidSchoolName(school["school.name"]):
            return school
    print(f"warning: division 1 school {find_school['school']} missing college data")
    return null


def getNFLPlayers(school):
    if school["school"] == "Texas A&M":
        api_url = "https://api.collegefootballdata.com/draft/picks?college=Texas%20A%26M"
    else:
        api_url = "https://api.collegefootballdata.com/draft/picks?college=" + school["school"]
    header = {'Authorization': 'Bearer Sj50mpfZQ19FDzPrTdh+QOUnUBLJIzDPDNll9x0zDof0N3WPl9qSoyTiCnF0zO+q'}
    response = requests.get(api_url, headers=header)
    players = []
    jsonified_data = response.json()
    for player in jsonified_data:
        cleaned_data = f"{player['name']} {player['position']} {player['overall']} {player['nflTeam']},"
        players.append(cleaned_data)
    school["nfl_players"] = "".join(players)


def getRecord(school):
    if school["school"] == "Texas A&M":
        api_url = "https://api.collegefootballdata.com/records?year=2021&team=Texas%20A%26M"
    else:
        api_url = "https://api.collegefootballdata.com/records?year=2021&team=" + school["school"]
    header = {'Authorization': 'Bearer Sj50mpfZQ19FDzPrTdh+QOUnUBLJIzDPDNll9x0zDof0N3WPl9qSoyTiCnF0zO+q'}
    response = requests.get(api_url, headers=header)
    jsonified_data = response.json()
    try:
        school["wins"] = jsonified_data[0]["total"]["wins"]
        school["losses"] = jsonified_data[0]["total"]["losses"]
        school["ties"] = jsonified_data[0]["total"]["ties"]
    except IndexError:
        school["wins"] = -1
        school["losses"] = -1
        school["ties"] = -1
        print(f"School {school['school']} with no record history found")


def getYards(school):
    if school["school"] == "Texas A&M":
        api_url = "https://api.collegefootballdata.com/stats/season?year=2021&team=Texas%20A%26M"
    else:
        api_url = "https://api.collegefootballdata.com/stats/season?year=2021&team=" + school["school"]
    header = {'Authorization': 'Bearer Sj50mpfZQ19FDzPrTdh+QOUnUBLJIzDPDNll9x0zDof0N3WPl9qSoyTiCnF0zO+q'}
    response = requests.get(api_url, headers=header)
    jsonified_data = response.json()
    for stat in jsonified_data:
        if stat["statName"] == "netPassingYards":
            school["passing_yards"] = stat["statValue"]
            return


def getRecruitingClassRank(school):
    if school["school"] == "Texas A&M":
        api_url = "https://api.collegefootballdata.com/recruiting/teams?year=2021&team=Texas%20A%26M"
    else:
        api_url = "https://api.collegefootballdata.com/recruiting/teams?year=2021&team=" + school["school"]
    header = {'Authorization': 'Bearer Sj50mpfZQ19FDzPrTdh+QOUnUBLJIzDPDNll9x0zDof0N3WPl9qSoyTiCnF0zO+q'}
    response = requests.get(api_url, headers=header)
    jsonified_data = response.json()
    try:
        school["recruiting_class_rank"] = jsonified_data[0]["rank"]
    except IndexError:
        school["recruiting_class_rank"] = -1


if __name__ == "__main__":
    load_dotenv()
    print("Query college data api for tuition info, etc")
    tuition_data = querySchoolSupplementaryData()
    print("Creating list of current division 1 schools and filling in matching tuition data")
    with open('../instances/schools.json') as data_file:
        valid_division = ["American Athletic", "ACC", "Big 12", "Conference USA", "FBS Independents", "Mid-American", "Mountain West", "Pac-12", "SEC", "Sun Belt", "Big Ten"]
        data = json.load(data_file)
        num_colleges_found = 0
        for school in data:
            if school["conference"] in valid_division:
                num_colleges_found += 1
                print(f"On school {num_colleges_found} of 116")
                school_data = getMatchingTuitionData(school, tuition_data)
                school["tuition"] = school_data["latest.cost.avg_net_price.overall"]
                school["size"] = school_data["latest.student.size"]
                school["school_full_name"] = school_data["school.name"]
                school["type"] = "public" if school_data["school.ownership"] == 1 else "private"
                getNFLPlayers(school)
                getRecord(school)
                getYards(school)
                getRecruitingClassRank(school)
                
                filled_schools.append(school)
        print(f"{len(filled_schools)} Division 1 teams found")

    with open("../instances/merged_schools2.json", "w") as f:
        json.dump(filled_schools, f)
    print("wrote div 1 schools")
    