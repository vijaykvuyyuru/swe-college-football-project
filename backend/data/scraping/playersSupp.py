import json
import urllib

import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from googlesearch import search
from selenium.webdriver.remote.webelement import WebElement
from sqlalchemy import null
from os.path import exists
import traceback
import logging

players_not_augmented = json.load(open('instances/unweighted_merged_players.json'))
stupid_schools = ["University of Kansas", "Liberty University", "University of Miami", "University of Virginia",
                  "The University of Tennessee-Knoxville", "Utah State University", "Vanderbilt University"]
clemson_schools = ["Clemson University", "Louisiana State University", "The Ohio State University", "University of Wyoming"]


def getPlayer(first_name:str, last_name:str, team:str):
    for player in players_not_augmented:
        if player["first_name"] is not None and player["first_name"].lower() == first_name.lower() and player["last_name"].lower() \
                == last_name.lower() and player["team"].lower() == team.lower():
            return player
    #print(f"Player {first_name} {last_name} on team {team} not found")
    return None


def getSchoolLogo(player):
    with open('instances/finished_schools.json') as data_file:
        schools = json.load(data_file)
        for school in schools:
            if school["school"] == player["team"]:
                return school["logos"][0]


def checkAlreadyGotPlayer(players, first_name, last_name):
    for player in players:
        if player["first_name"].lower() == first_name.lower() and player["last_name"].lower() == last_name.lower():
            return True
    return False


# Scrape player image and weight off of roster websites
def scrapePlayerData(school, players):
    page = requests.get(school["url"])
    if school["school"] in stupid_schools or school["school"] in clemson_schools: #TODO add clemson schools
        print("manual school")
        return
    if page.status_code != 200:
        print(f"2020 roster for {school['school']} link is invalid, get it manually")
        return
    driver = webdriver.Chrome("C:/Users/vijay/Downloads/chromedriver.exe")
    driver.get(school["url"])
    all_names = driver.find_elements(by=By.CLASS_NAME, value='sidearm-roster-player-name')
    all_weights = driver.find_elements(by=By.CLASS_NAME, value='sidearm-roster-player-weight')
    tableSchool = False

    # if len(all_imgs) == 0:
    #     tableSchool = True
    if len(all_names) == 0 and len(all_weights) == 0:
        all_names = driver.find_elements(by=By.CLASS_NAME, value='sidearm-table-player-name')
        all_weights = driver.find_elements(by=By.CLASS_NAME, value='rp_weight')
        tableSchool = True
    else:
        soup = BeautifulSoup(requests.get(school["url"]).text, "html.parser")
        res = json.loads(soup.find("script", {"type": "application/ld+json"}).string)["item"]
        images = []
        for player in res:
            try:
                images.append(player['image']['url'])
            except TypeError:
                images.append(None)

    if len(all_names) == 0 and len(all_weights) == 0:
        print(f"Stupid school found: {school['school']}")
        return
    if tableSchool:
        for name, weight in zip(all_names, all_weights):
            if "\n" in name.text:
                clean = name.text.split("\n")[1]
                names = clean.split(" ")
            else:
                clean = name.text
                names = name.text.split(" ")
            if checkAlreadyGotPlayer(players, names[0], names[1]):
                print(f"already got player {names[0]} {names[1]}")
                continue
            player = getPlayer(names[0], names[1], school['school_team_name'])
            if player:
                try:
                    player["picture"] = getImageFromTable(player, page, clean, school["url"])
                    if player["weight"] is null:
                        player["weight"] = weight
                    players.append(player)
                except (IndexError, urllib.error.URLError):
                    print(f"{player['first_name']} {player['last_name']} parsing error, setting picture to school logo")
                    player["picture"] = getSchoolLogo(player)
                    players.append(player)
    else:
        for name, weight, img in zip(all_names, all_weights, images):
            if "\n" in name.text:
                clean = name.text.split("\n")[1]
                names = clean.split(" ")
            else:
                clean = name.text
                names = name.text.split(" ")
            if len(names) > 1:
                if checkAlreadyGotPlayer(players, names[0], names[1]):
                    print(f"already got player {names[0]} {names[1]}")
                    continue
                player = getPlayer(names[0], names[1], school['school_team_name'])
                if player:
                    try:
                        if isinstance(img, WebElement):
                            if img.get_attribute("src") is None:
                                player["picture"] = getSchoolLogo(player)
                            else:
                                player["picture"] = img.get_attribute("src")
                        else:
                            player["picture"] = img
                        if player["weight"] is null:
                            player["weight"] = weight
                        players.append(player)
                        print(f"Appended player {player['first_name']} {player['last_name']}")
                    except IndexError:
                        print(f"{player['first_name']} {player['last_name']} parsing error, skipping")


# Get image of a player that does not have it on the main page
def getImageFromTable(player, page, player_full_name:str, url:str):
    soup = BeautifulSoup(page.content, 'html.parser')
    print(f"finding image for player called {player_full_name}")

    schoolDom = getSchoolDomain(url)
    for link in search(f"{player['first_name']} {player['last_name']} {player['team']}", tld="us", num=10, stop=10,
                       pause=2):
        if schoolDom in link:
            print(link)
            player_page = requests.get(link)
            break
    try:
        player_soup = BeautifulSoup(player_page.content, 'html.parser')
    except UnboundLocalError:
        print(f"Error getting image for {player['first_name']} {player['last_name']}")
        return getSchoolLogo(player)
    img = player_soup.find_all('div', class_="sidearm-roster-player-image")
    dirty_img_link = str(img).split("src=")[1].split("/>")[0]
    cleaned_image_link = dirty_img_link[1: len(dirty_img_link) - 1]
    if cleaned_image_link[0] == "/":
        cleaned_image_link = getSchoolURL(url) + cleaned_image_link
    #print(cleaned_image_link)
    return cleaned_image_link


def getSchoolDomain(url):
    start = url.index("://")+3
    stop = url.index(".com")
    return url[start:stop]


def getSchoolURL(url):
    stop = url.index(".com")
    return url[:stop+4]


# Load all palyers that have already been supplemented to avoid repeated queries
def loadSuppedPlayer():
    with open('instances/finished_normal_players.json') as player_data_file:
        player_data = json.load(player_data_file)
        return player_data


def loadCompletedSchools():
    with open('instances/finished_schools.json') as school_data_file:
        school_data = json.load(school_data_file)
        return school_data


if __name__ == "__main__":
    with open('../instances/schools_urls.json') as data_file:
        data = json.load(data_file)
        if exists('../instances/finished_normal_players.json'):
            players = loadSuppedPlayer()
        else:
            players = []
        if exists('../instances/finished_schools.json'):
            completed_schools = loadCompletedSchools()
        else:
            completed_schools = []
        try:
            for (i, school) in enumerate(data):
                print(f"On school {i} {school['school']}")
                if school["school"] not in completed_schools:
                    scrapePlayerData(school, players)
                    completed_schools.append(school["school"])
                else:
                    print(f"Skipped {school['school']}")
        except Exception as e:
            logging.error(traceback.format_exc())
        finally:
            with open("../instances/finished_normal_players.json", "w") as f:
                json.dump(players, f)
            with open("../instances/finished_schools.json", "w") as w:
                json.dump(completed_schools, w)

