import json
from googlesearch import search

#Generate url for each schools roster page
def getSchoolAndRoster(school):
    for link in search(f"{school['school_full_name']} football roster", tld="us", num=10, stop=10, pause=2):
        if 'roster' in link:
            print(f"link: {link} for {school['school_full_name']}")
            school_url = {
                "school": school["school_full_name"],
                "url": f"{link}/2020",
                "school_team_name": school["school"]
            }
            return school_url
    print(f"could not find school roster page for {school['school_full_name']}")


if __name__ == "__main__":
    print("Query google for roster info")
    urls = []
    with open('../instances/finished_schools.json') as data_file:
        data = json.load(data_file)

        for school in data:
            urls.append(getSchoolAndRoster(school))
    with open("../instances/schools_urls.json", "w") as f:
        json.dump(urls, f)
    print("wrote players")
