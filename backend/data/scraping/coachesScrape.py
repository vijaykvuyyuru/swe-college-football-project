import json
import requests
from bs4 import BeautifulSoup
import re

# Coaches with non-standard urls are gotten from here
odd_names = {
    "Blake Anderson": "https://en.wikipedia.org/wiki/Blake_Anderson_(American_football)",
    "Matt Wells": "https://en.wikipedia.org/wiki/Matt_Wells_(American_football_coach)",
    "Justin Wilcox": "https://en.wikipedia.org/wiki/Justin_Wilcox_(American_football)",
    "Tom Allen": "https://en.wikipedia.org/wiki/Tom_Allen_(American_football)",
    "Matt Campbell": "https://en.wikipedia.org/wiki/Matt_Campbell_(American_football_coach)",
    "Steve Campbell": "https://en.wikipedia.org/wiki/Steve_Campbell_(American_football)",
    "Bill Clark": "https://en.wikipedia.org/wiki/Bill_Clark_(American_football)",
    "Geoff Collins": "https://en.wikipedia.org/wiki/Geoff_Collins_(American_football)",
    "Ryan Day": "https://en.wikipedia.org/wiki/Ryan_Day_(American_football)",
    "Manny Diaz": "https://en.wikipedia.org/wiki/Manny_Diaz_(American_football)",
    "Shawn Elliott": "https://en.wikipedia.org/wiki/Shawn_Elliott_(American_football)",
    "James Franklin": "https://en.wikipedia.org/wiki/James_Franklin_(American_football_coach)",
    "Will Hall": "https://en.wikipedia.org/wiki/Will_Hall_(American_football)",
    "Mike Houston": "https://en.wikipedia.org/wiki/Mike_Houston_(American_football)",
    "Charles Huff": "https://en.wikipedia.org/wiki/Charles_Huff_(American_football)",
    "Brian Kelly": "https://en.wikipedia.org/wiki/Brian_Kelly_(American_football_coach)",
    "Mike Leach": "https://en.wikipedia.org/wiki/Mike_Leach_(American_football_coach)",
    "Tim Lester": "https://en.wikipedia.org/wiki/Tim_Lester_(American_football_coach)",
    "Sean Lewis": "https://en.wikipedia.org/wiki/Sean_Lewis_(American_football)",
    "Chuck Martin": "https://en.wikipedia.org/wiki/Chuck_Martin_(American_football)",
    "Doug Martin": "https://en.wikipedia.org/wiki/Doug_Martin_(American_football_coach)",
    "David Shaw": "https://en.wikipedia.org/wiki/David_Shaw_(American_football)",
    "Jonathan Smith": "https://en.wikipedia.org/wiki/Jonathan_Smith_(American_football_coach)"
}

skipped_coaches = []

# Scrape the coach salary, overall record, and image from the wikimedia page
def getCoachInfo(first_name: str, last_name: str, coach):
    if f"{first_name} {last_name}" in odd_names:
        page = requests.get(odd_names[f"{first_name} {last_name}"])
    else:
        page = requests.get(f"https://en.wikipedia.org/wiki/{first_name}_{last_name}")
    soup = BeautifulSoup(page.content, 'html.parser')
    try:
        print(f"Searching for coach: {first_name} {last_name}")
        infobox = soup.find('table', {'class': 'infobox'})
        info_lines = infobox.find_all('tr')
        for line in info_lines:
            if ".jpg" in str(line) or ".png" in str(line):
                elems = str(line).split()
                for string in elems:
                    if "src" in string and not "srcset" in string:
                        image_url = string[5:len(string)-1]
                        coach["image"] = image_url
            if "Annual salary" in str(line):
                elems = str(line).split()
                for (i, string) in enumerate(elems):
                    if '$' in string:
                        amount = string.split("$")
                        if i+1 < len(elems) and "million" in elems[i+1]:
                            if "a" in amount[1]:
                                amount[1] = amount[1].split(">")[1]
                            salary = int(float(amount[1]) * 1_000_000)
                        else:
                            cleaned_amount = amount[1].split("<")
                            if len(cleaned_amount[0]) == 0:
                                numeric_filter = filter(str.isdigit, cleaned_amount[1])
                            else:
                                numeric_filter = filter(str.isdigit, cleaned_amount[0])
                            numeric_string = "".join(numeric_filter)
                            salary = int(numeric_string.replace(',', ''))
                        print(salary)
                        coach["salary"] = salary
            if "Record" in str(line):
                elems = str(line).split()
                for string in elems:
                    if 'class="infobox-data"' in string:
                        record_string = re.split('<|>', string)
                        record_list = record_string[1].split("–")
                        record_wins = int(record_list[0])
                        record_losses = int(record_list[1])
                        print(f"wins: {record_wins}")
                        print(f"losses: {record_losses}")
                        coach["wins"] = record_wins
                        coach["losses"] = record_losses
            elif "Overall" in str(line):
                elems = str(line).split()
                for string in elems:
                    if 'class="infobox-data"' in string:
                        record_string = re.split('<|>', string)
                        record_list = record_string[1].split("–")
                        if len(record_list[0]) > 3:
                            temp_list = record_list[0].split("-")
                            record_list[0] = temp_list[0]
                            record_list.append(temp_list[1])
                        overall_record_wins = int(record_list[0])
                        overall_record_losses = int(record_list[1])
                        print(f"overall wins: {overall_record_wins}")
                        print(f"overall losses: {overall_record_losses}")
                        coach["overall_wins"] = overall_record_wins
                        coach["overall_losses"] = overall_record_losses
    # If parsing fails for any reason, print coach and add to list
    except AttributeError:
        print(f"Wikipedia page for {first_name} {last_name} not found or salary not found")
        skipped_coaches.append(f"{first_name} {last_name}")


if __name__ == "__main__":
    coaches = []
    with open('../instances/current_coaches.json') as data_file:
        data = json.load(data_file)
        # Loop through each coach and add extra data
        for (i, coach) in enumerate(data):
            print(f"On coach {i}")
            getCoachInfo(coach["first_name"], coach["last_name"], coach)
            coaches.append(coach)
        print(f"{len(skipped_coaches)} coaches skipped")
        print(skipped_coaches)
    with open("../instances/finished_coaches.json", "w") as f:
        json.dump(coaches, f)
