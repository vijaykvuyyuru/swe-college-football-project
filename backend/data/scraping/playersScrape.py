import json
import requests
from dotenv import load_dotenv
import os
from math import ceil
from os.path import exists

from sqlalchemy import null

# Query API to get player usage statistics
def getPlayerUsage(player):
    # if player["school"] == "Texas A&M":
    #     api_url = "https://api.collegefootballdata.com/recruiting/teams?year=2021&team=Texas%20A%26M"
    # else:
    #     api_url = "https://api.collegefootballdata.com/recruiting/teams?year=2021&team=" + school["school"]
    api_url = "https://api.collegefootballdata.com/player/usage?year=2021&playerId=" + player["id"]
    header = {'Authorization': 'Bearer Sj50mpfZQ19FDzPrTdh+QOUnUBLJIzDPDNll9x0zDof0N3WPl9qSoyTiCnF0zO+q'}
    response = requests.get(api_url, headers=header)
    jsonified_data = response.json()
    try:
        player["overall_usage"] = jsonified_data[0]["usage"]["overall"]
        player["passes"] = jsonified_data[0]["usage"]["pass"]
        player["rushes"] = jsonified_data[0]["usage"]["rush"]
        player["first_down"] = jsonified_data[0]["usage"]["firstDown"]
        player["second_down"] = jsonified_data[0]["usage"]["secondDown"]
        player["third_down"] = jsonified_data[0]["usage"]["thirdDown"]
        return True
    except IndexError:
        return False


if __name__ == "__main__":
    players = []
    with open('../instances/players.json') as data_file:
        data = json.load(data_file)
        for (i, player) in enumerate(data):
            print(f"On player {i}")
            used = getPlayerUsage(player)
            if used:
                players.append(player)
            else:
                print("did not find player usage")
    with open("../instances/unweighted_merged_players.json", "w") as f:
        json.dump(players, f)