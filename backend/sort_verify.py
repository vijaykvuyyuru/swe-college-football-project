# python 3.7.2
# helper functions in testing to determine whether a list
# is sorted
# sourced from StackOverflow
# https://stackoverflow.com/questions/3755136/pythonic-way-to-check-if-a-list-is-sorted-or-not


def is_sorted_asc(L: list) -> bool:
    """
    determine whether a list is sorted in ascending order

    @param L: list

    @return boolean: True if list is sorted, false otherwise
    """
    return all(L[i] <= L[i + 1] for i in range(len(L) - 1))


def is_sorted_asc_string(L: list) -> bool:
    """
    determine whether a list of strings is sorted in ascending order

    @param L: list

    @return boolean: True if list is sorted, false otherwise
    """

    return all(L[i].lower().replace("'", "") <= L[i + 1].lower().replace("'", "") for i in range(len(L) - 1))

def is_sorted_desc(L: list) -> bool:
    """
    determine whether a list is sorted in ascending order

    @param L: list

    @return boolean: True if list is sorted, false otherwise
    """
    return all(L[i] >= L[i + 1] for i in range(len(L) - 1))

def is_sorted_desc_string(L: list) -> bool:
    """
    determine whether a list of strings is sorted in ascending order

    @param L: list

    @return boolean: True if list is sorted, false otherwise
    """
    # return all(L[i].lower() >= L[i + 1].lower() for i in range(len(L) - 1))
    for i in range(len(L)-1):
        if not L[i].lower().replace("'", "") >= L[i+1].lower().replace("'", ""):
            return False
    return True