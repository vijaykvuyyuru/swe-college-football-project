from operator import truediv
import flask
import sqlalchemy
from flask import Flask, request, make_response, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, Column, String, Integer, null
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from marshmallow import fields, post_dump
from dotenv import load_dotenv

import os
import json
import enum

classes = ["", "freshman", "sophomore", "junior", "senior"]
# classes = {1: "freshman", 2: "sophomore", 3: "junior", 4: "senior"}
class ClassEnum(enum.Enum):
    freshman = 1
    sophomore = 2
    junior = 3
    senior = 4


def init_db(app):
    """
    Sourced from TexasVotes (Jefferson Ye's Group)
    repo: https://gitlab.com/forbesye/fitsbits
    """
    load_dotenv()
    app.debug = True
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("DATABASE_URI")

    db = SQLAlchemy(app)
    return db


def init():
    app = Flask(__name__)
    CORS(app)
    db = init_db(app)
    ma = Marshmallow(app)

    return app, db, ma


app, db, ma = init()


class School(db.Model):
    """
    represents a School instance in the database.
    """

    __tablename__ = "schools"
    id = db.Column(db.Integer, primary_key=True)
    school = db.Column(db.String())
    school_nickname = db.Column(db.String())
    mascot = db.Column(db.String())
    color = db.Column(db.String())
    conference = db.Column(db.String())
    division = db.Column(db.String())
    size = db.Column(db.Integer)
    tuition = db.Column(db.Integer)
    type = db.Column(db.String())
    location = db.Column(db.String())
    wins = db.Column(db.Integer)
    losses = db.Column(db.Integer)
    ties = db.Column(db.Integer)
    recruiting_class_rank = db.Column(db.Integer)
    nfl_players = db.Column(db.String())
    passing_yards = db.Column(db.Integer)
    logo = db.Column(db.String())

    def __repr__(self):
        return f"<School {self.id}: {self.school}>"


class SchoolSchema(ma.SQLAlchemySchema):
    class Meta:
        ordered = True
        model = School

    id = ma.auto_field()
    school = ma.auto_field()
    school_nickname = ma.auto_field()
    mascot = ma.auto_field()
    color = ma.auto_field()
    conference = ma.auto_field()
    division = ma.auto_field()
    size = ma.auto_field()
    tuition = ma.auto_field()
    type = ma.auto_field()
    location = ma.auto_field()
    wins = ma.auto_field()
    losses = ma.auto_field()
    ties = ma.auto_field()
    recruiting_class_rank = ma.auto_field()
    nfl_players = ma.auto_field()
    passing_yards = ma.auto_field()
    logo = ma.auto_field()


class Player(db.Model):
    __tablename__ = "players"
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String())
    last_name = db.Column(db.String())
    year = db.Column(db.Integer)
    position = db.Column(db.String())
    height = db.Column(db.Integer)
    weight = db.Column(db.Integer)
    jersey = db.Column(db.Integer)
    home_city = db.Column(db.String())
    home_state = db.Column(db.String())
    overall_usage = db.Column(db.Float)
    passes = db.Column(db.Float)
    rushes = db.Column(db.Float)
    first_down = db.Column(db.Float)
    second_down = db.Column(db.Float)
    third_down = db.Column(db.Float)
    picture = db.Column(db.String())

    # connected components
    school_id = db.Column(Integer, db.ForeignKey("schools.id"))
    coach_id = db.Column(Integer, db.ForeignKey("coaches.id"))
    school_logo = db.Column(db.String())
    coach_first_name = db.Column(db.String())
    coach_last_name = db.Column(db.String())

    def __repr__(self):
        return f"<Player {self.id}: {self.first_name} {self.last_name}>"


class PlayerSchema(ma.SQLAlchemySchema):
    class Meta:
        ordered = True
        model = Player

    id = ma.auto_field()
    first_name = ma.auto_field()
    last_name = ma.auto_field()
    year = ma.auto_field()
    position = ma.auto_field()
    height = ma.auto_field()
    weight = ma.auto_field()
    jersey = ma.auto_field()
    home_city = ma.auto_field()
    home_state = ma.auto_field()
    # passing_yards = ma.auto_field()
    # rushing_yards = ma.auto_field()
    first_down = ma.auto_field()
    second_down = ma.auto_field()
    third_down = ma.auto_field()
    # standard_downs = ma.auto_field()
    # passing_downs = ma.auto_field()
    picture = ma.auto_field()

    # connected components
    school_id = ma.auto_field()
    coach_id = ma.auto_field()
    school_logo = ma.auto_field()
    coach_first_name = ma.auto_field()
    coach_last_name = ma.auto_field()


class Coach(db.Model):
    __tablename__ = "coaches"
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String())
    last_name = db.Column(db.String())
    hire_date = db.Column(db.DateTime)
    current_wins = db.Column(db.Integer)
    current_losses = db.Column(db.Integer)
    overall_wins = db.Column(db.Integer)
    overall_losses = db.Column(db.Integer)
    salary = db.Column(db.Integer)
    image = db.Column(db.String())

    # connected components
    school_id = db.Column(Integer, db.ForeignKey("schools.id"))
    school_logo = db.Column(db.String())

    def __repr__(self):
        return f"<Coach {self.id}: {self.first_name} {self.last_name}>"


class CoachSchema(ma.SQLAlchemySchema):
    class Meta:
        ordered = True
        model = Coach

    id = ma.auto_field()
    first_name = ma.auto_field()
    last_name = ma.auto_field()
    hire_date = ma.auto_field()
    current_wins = ma.auto_field()
    current_losses = ma.auto_field()
    overall_wins = ma.auto_field()
    overall_losses = ma.auto_field()
    salary = ma.auto_field()
    image = ma.auto_field()

    # connected components
    school_id = ma.auto_field()
    school_logo = ma.auto_field()


class CoachHistory(db.Model):
    __tablename__ = "coaches_history"
    id = db.Column(db.Integer, primary_key=True)
    season = db.Column(db.Integer)
    wins = db.Column(db.Integer)
    losses = db.Column(db.Integer)
    ties = db.Column(db.Integer)
    preseason_rank = db.Column(db.Integer)
    postseason_rank = db.Column(db.Integer)

    # connected components
    coach_id = db.Column(Integer, db.ForeignKey("coaches.id"))
    school_id = db.Column(Integer, db.ForeignKey("schools.id"))


class CoachHistorySchema(ma.SQLAlchemySchema):
    class Meta:
        ordered = True
        model = CoachHistory

    id = ma.auto_field()
    season = ma.auto_field()
    wins = ma.auto_field()
    losses = ma.auto_field()
    ties = ma.auto_field()
    preseason_rank = ma.auto_field()
    postseason_rank = ma.auto_field()

    # connected components
    coach_id = ma.auto_field()
    school_id = ma.auto_field()


def push_school(db, json_data: dict):
    entry = dict()
    entry["school"] = json_data["school_full_name"]
    entry["school_nickname"] = json_data["school"]
    entry["mascot"] = json_data["mascot"]
    entry["color"] = json_data["color"]
    entry["conference"] = json_data["conference"]
    entry["division"] = json_data["division"]
    entry["size"] = json_data["size"]
    entry["tuition"] = json_data["tuition"]
    entry["type"] = json_data["type"]
    entry["location"] = json_data["location"]["name"]
    entry["wins"] = json_data["wins"]
    entry["losses"] = json_data["losses"]
    entry["recruiting_class_rank"] = json_data["recruiting_class_rank"]
    entry["nfl_players"] = json_data["nfl_players"]
    entry["passing_yards"] = json_data["passing_yards"]
    entry["logo"] = json_data["logos"][0]
    instance = School(**entry)
    db.session.add(instance)
    db.session.commit()


def get_matching_school(db, school_name: str):
    return db.session.query(School).filter(School.school_nickname == school_name).one()


def get_matching_school_logo(db, school_name: str):
    return (
        db.session.query(School)
        .filter(School.school_nickname == school_name)
        .one()
        .logo
    )


def push_coaches(db, json_data: dict):
    entry = dict()
    entry["first_name"] = json_data["first_name"]
    entry["last_name"] = json_data["last_name"]
    entry["hire_date"] = json_data["hire_date"]
    if "wins" in json_data:
        entry["current_wins"] = json_data["wins"]
    else:
        entry["current_wins"] = 0
    if "losses" in json_data:
        entry["current_losses"] = json_data["losses"]
    else:
        entry["current_losses"] = 0

    if "overall_wins" in json_data:
        entry["overall_wins"] = json_data["overall_wins"]
    else:
        entry["overall_wins"] = 0
    if "overall_losses" in json_data:
        entry["overall_losses"] = json_data["overall_losses"]
    else:
        entry["overall_losses"] = 0

    if "salary" in json_data:
        entry["salary"] = json_data["salary"]
    else:
        entry["salary"] = 0

    if "image" in json_data:
        entry["image"] = json_data["image"]
    else:
        entry["image"] = get_matching_school_logo(
            db, json_data["seasons"][-1]["school"]
        )

    matching_school = get_matching_school(db, json_data["seasons"][-1]["school"])
    entry["school_id"] = matching_school.id
    entry["school_logo"] = matching_school.logo
    instance = Coach(**entry)
    db.session.add(instance)
    db.session.commit()


def get_matching_coach_id(db, coach_firstname: str, coach_lastname: str):
    return (
        db.session.query(Coach)
        .filter(Coach.first_name == coach_firstname)
        .filter(Coach.last_name == coach_lastname)
        .one()
        .id
    )


def get_matching_coach(db, school_id: str):
    try:
        return db.session.query(Coach).filter(Coach.school_id == school_id).one()
    except sqlalchemy.exc.MultipleResultsFound as e:
        print(f"Multiple coaches found for school id{school_id}")
        return db.session.query(Coach).filter(Coach.school_id == school_id).first()


def push_historical_data(db, json_data: dict):
    coach_id = get_matching_coach_id(
        db, json_data["first_name"], json_data["last_name"]
    )
    for season in json_data["seasons"]:
        if season["year"] == 2021:
            break
        entry = dict()
        entry["school_id"] = get_matching_school(db, season["school"]).id
        entry["coach_id"] = coach_id
        entry["season"] = season["year"]
        entry["wins"] = season["wins"]
        entry["losses"] = season["losses"]
        entry["ties"] = season["ties"]
        if season["preseason_rank"] is null:
            entry["preseason_rank"] = 0
        else:
            entry["preseason_rank"] = season["preseason_rank"]

        if season["postseason_rank"] is null:
            entry["postseason_rank"] = 0
        else:
            entry["postseason_rank"] = season["postseason_rank"]
        instance = CoachHistory(**entry)
        db.session.add(instance)
        db.session.commit()


def push_players(db, json_data: dict):
    entry = dict()
    entry["first_name"] = json_data["first_name"]
    entry["last_name"] = json_data["last_name"]
    entry["year"] = json_data["year"]
    entry["position"] = json_data["position"]
    entry["height"] = json_data["height"]
    entry["weight"] = json_data["weight"]
    entry["jersey"] = json_data["jersey"]
    entry["home_city"] = json_data["home_city"]
    entry["home_state"] = json_data["home_state"]
    entry["overall_usage"] = json_data["overall_usage"]
    entry["passes"] = json_data["passes"]
    entry["rushes"] = json_data["rushes"]
    entry["first_down"] = json_data["first_down"]
    entry["second_down"] = json_data["second_down"]
    entry["third_down"] = json_data["third_down"]
    entry["picture"] = json_data["picture"]

    matching_school = get_matching_school(db, json_data["team"])
    matching_coach = get_matching_coach(db, matching_school.id)
    entry["school_id"] = matching_school.id
    entry["school_logo"] = matching_school.logo
    entry["coach_first_name"] = matching_coach.first_name
    entry["coach_last_name"] = matching_coach.last_name
    entry["coach_id"] = matching_coach.id
    instance = Player(**entry)
    db.session.add(instance)
    db.session.commit()


def populate(db, datapath: str, filename: str, push_historical=False):
    """
    Function to populate the database with all instances sourced from a
    specified input JSON file
    This method was sourced from https://gitlab.com/thomaspeavler2/cs373-project/-/blob/dev/back_end/models.py
    """
    with open(str(datapath + filename), "r") as fp:
        objs = json.load(fp)

    assert type(objs) == list
    assert len(objs) > 0

    if "school" in filename:
        push_func = push_school
    elif "player" in filename:
        push_func = push_players
    elif "coach" in filename and not push_historical:
        push_func = push_coaches
    elif "coach" in filename and push_historical:
        push_func = push_historical_data
    else:
        push_func = None

    if push_func is not None:
        for obj in objs:
            push_func(db, obj)
        return True
        db.session.commit()
    else:
        return False


def reset_db():
    # Be VERYYY careful with this...
    db.session.remove()
    print("Removed from current session")
    db.drop_all()
    print("Dropped all")
    db.create_all()
    print("Database reset")


if __name__ == "__main__":
    load_dotenv()
    print(os.getenv("DATABASE_URI"))
    print("Initiating db push...")
    reset_db()
    path = "data/instances/"
    teams_result = populate(db, path, "finished_schools.json")
    if teams_result:
        print("Populated team data")
    coaches_result = populate(db, path, "finished_coaches.json")
    if coaches_result:
        print("Populated coaches data")
    players_result = populate(db, path, "finished_players.json")
    if players_result:
        print("Populated player data")
    coaches_history_result = populate(
        db, path, "finished_coaches.json", push_historical=True
    )
    if coaches_history_result:
        print("Populated coaches history data")
    db.session.commit()
    print("finished db push")
