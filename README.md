## Group Members

| Name | EID | GitLab ID |
| --- | --- | --- |
| Vijay Vuyyuru | vkv286 | vijaykvuyyuru |
| Brock Moore | bam4865 | brockm2018 |
| Blake Chambers | bwc668 | blakeC30 |
| Abhinav Mugunda | am85942 | am85942 |
| Matt Dombrowski | mrd2878 | mattdombrowski21 |

## Git SHA
Phase I: ea9aff7b82f1f74c4c67edc6b70586e0a6841598

Phase II: 947738e196f996a52ff175e586df603d53c1c744

Phase III: 631f468364456b4faa8625c78e6d4691a49b37d4

Phase IV: db5167eb2fdef1fa731987aa445c8d07fd69479e

## Project Leaders
Phase I: Vijay Vuyyuru

Phase II: Matt Dombrowski

Phase III: Brock Moore

Phase IV: Blake Chambers

## GitLab Pipelines
https://gitlab.com/vijaykvuyyuru/swe-college-football-project/-/pipelines
## Postman API
For full documentation
on our API, please visit our Postman link at
https://documenter.getpostman.com/view/19756764/UVkpQGPx

## Website
Check us out at https://tuitionball.me

## Estimated Completion Times
### Phase I
| Name | Estimated Completion Time | Actual Completion Time |
| --- | --- | --- |
| Vijay Vuyyuru | 10 hours | 20 hours |
| Brock Moore | 10 hours | 15 hours |
| Blake Chambers | 10 hours | 20 hours |
| Abhinav Mugunda | 10 hours | 15 hours |
| Matt Dombrowski | 10 hours | 21 hours  |

### Phase II
| Name | Estimated Completion Time | Actual Completion Time |
| --- |---------------------------|------------------------|
| Vijay Vuyyuru | 30 hours                  | 40 hours               |
| Brock Moore | 20 hours                  | 25 hours               |
| Blake Chambers | 20 hours                  | 25 hours               |
| Abhinav Mugunda | 20 hours                  | 0 hours                |
| Matt Dombrowski | 30 hours                  | 32 hours               |

### Phase III
| Name | Estimated Completion Time | Actual Completion Time |
| --- |---------------------------|------------------------|
| Vijay Vuyyuru | 10 hours                  | 10 hours               |
| Brock Moore | 10 hours                  | 18 hours               |
| Blake Chambers | 10 hours                  | 15 hours               |
| Abhinav Mugunda | 0 hours                  | 0 hours                |
| Matt Dombrowski | 10 hours                  | 10 hours               |

### Phase IV
| Name | Estimated Completion Time | Actual Completion Time |
| --- |---------------------------|------------------------|
| Vijay Vuyyuru | 5 hours                  | 5 hours               |
| Brock Moore | 10 hours                  | 10 hours               |
| Blake Chambers | 10 hours                  | 10 hours               |
| Abhinav Mugunda | 0 hours                  | 0 hours                |
| Matt Dombrowski | 10 hours                  | 10 hours               |
## Comments


### References
Phase I:
Some of our Navbar and splash page was pulled from the following: https://gitlab.com/thomaspeavler2/cs373-project/-/tree/dev/front_end/src
The table used in the basics page was pulled from the following: https://mui.com/components/tables/

Phase II:
Some of our gitlab.ci.yml were sourced from other sources. This has been cited inside the document. This is also true of our backend/models.py. The dockerfile, uwsgi.ini, and nginx.conf were sourced from https://github.com/forbesye/cs373/blob/main/Flask_AWS_Deploy.md. 
We also referenced https://gitlab.com/thomaspeavler2/cs373-project/ for some aspects of the backend work such as defining columns in the database. 

Phase III:
We referenced https://gitlab.com/mayankdaruka/bookipedia/ for some aspects of the backend work when implementing search.