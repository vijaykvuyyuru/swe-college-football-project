docker :
	docker build -t tuition-ball frontend/
	docker run -t -a STDOUT -p 3000:3000 -v ./frontend:/app -v /app/node_modules -e CHOKIDAR_USEPOLLING=true tuition-ball

docker-compose :
	docker-compose up

build-backend :
	docker build -t backend backend/

build-frontend :
	docker build -t frontend frontend/

test-backend :
	docker run -v ./backend:/app backend python3 test_backend.py

python-unit-tests:
	python3 backend/tests.py

format:
	python3 -m black ./backend/*.py
	python3 -m black ./frontend/*.py

